#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef __APPLE__
#define GL_SILENCE_DEPRECATION
#endif /* __APPLE__ */
#include <GLFW/glfw3.h>

#include "system/graphics.h"
#include "system/input.h"

#define INITIAL_WIDTH   640
#define INITIAL_HEIGHT  480
#define MAX_DEGREES     360.0f

#ifndef M_PI
#define M_PI 3.141f
#endif /* M_PI */

#define DEG_TO_RAD  180.0f * M_PI

static GLuint surface_dl;

static struct engine_state {
    bool is_running;
} engine_state;

static void draw_tile_block()
{
    glBegin(GL_QUADS);
        glColor3f(0.2f, 0.2f, 0.2f);
        glVertex3f(1.0f, 1.0f, 0.5f);
        glVertex3f(1.0f, -1.0f, 0.5f);
        glColor3f(0.4f, 0.4f, 0.4f);
        glVertex3f(-1.0f, -1.0f, 0.5f);
        glVertex3f(-1.0f, 1.0f, 0.5f);

        glColor3f(0.2f, 0.2f, 0.2f);
        glVertex3f(1.0f, 1.0f, -0.5f);
        glVertex3f(1.0f, -1.0f, -0.5f);
        glColor3f(0.4f, 0.4f, 0.4f);
        glVertex3f(1.0f, -1.0f, 0.5f);
        glVertex3f(1.0f, 1.0f, 0.5f);

        glColor3f(0.2f, 0.2f, 0.2f);
        glVertex3f(-1.0f, 1.0f, -0.5f);
        glVertex3f(-1.0f, -1.0f, -0.5f);
        glColor3f(0.4f, 0.4f, 0.4f);
        glVertex3f(1.0f, -1.0f, -0.5f);
        glVertex3f(1.0f, 1.0f, -0.5f);
        
        glColor3f(0.2f, 0.2f, 0.2f);
        glVertex3f(-1.0f, 1.0f, 0.5f);
        glVertex3f(-1.0f, -1.0f, 0.5f);
        glColor3f(0.4f, 0.4f, 0.4f);
        glVertex3f(-1.0f, -1.0f, -0.5f);
        glVertex3f(-1.0f, 1.0f, -0.5f);
        
        glColor3f(0.2f, 0.2f, 0.2f);
        glVertex3f(1.0f, 1.0f, -0.5f);
        glVertex3f(1.0f, 1.0f, 0.5f);
        glColor3f(0.4f, 0.4f, 0.4f);
        glVertex3f(-1.0f, 1.0f, 0.5f);
        glVertex3f(-1.0f, 1.0f, -0.5f);
        
        glColor3f(0.2f, 0.2f, 0.2f);
        glVertex3f(1.0f, -1.0f, 0.5f);
        glVertex3f(1.0f, -1.0f, -0.5f);
        glColor3f(0.4f, 0.4f, 0.4f);
        glVertex3f(-1.0f, -1.0f, -0.5f);
        glVertex3f(-1.0f, -1.0f, 0.5f);
    glEnd();
}

static void draw_tile(float x, float y, float z)
{
    glPushMatrix();
        glTranslatef(x, y, z);
        glCallList(surface_dl);
    glPopMatrix();
}

int main(int argc, char *argv[])
{
    float velocity = 0.1f;
    const float floor_height = -3.0f;
    const int floor_start = -6;
    const int floor_depth = 6;
    
    bool is_fullscreen = false;
    
    int i;
    float x_pos = 0.0f, y_pos = 0.0f, z_pos = -1.0f;
    float view_angle = 0.0f;
    float object_angle = 0.0f;
    bool moving = false;
    float hand_pos_x = 0.0f;
    float hand_pos_y = 0.0f;
    float hand_x_phase = 0.0f;
    float hand_y_phase = 0.0f;
    bool fullscreen_toggle_done = true;
    
    engine_state.is_running = true;

    graphics_init(INITIAL_WIDTH, INITIAL_HEIGHT, "OpenGL", is_fullscreen);
    input_init();
    
    /* scene init */
    
    glClearColor(0.0, 0.0, 0.2, 0.0);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_FRONT);
    
    surface_dl = glGenLists(1);
    glNewList(surface_dl, GL_COMPILE);
        draw_tile_block();
    glEndList();
    
    /* scene init end */

	while (graphics_running()) {
        /* scene process input */
        
        if (shift_held) {
            velocity = 0.2f;
        } else {
            velocity = 0.1f;
        }
        
        if (directions & DIR_LEFT) {
            x_pos += cos(view_angle / DEG_TO_RAD) * velocity;
            z_pos += sin(view_angle / DEG_TO_RAD) * velocity;
            moving = true;
        }
        
        if (directions & DIR_RIGHT) {
            x_pos -= cos(view_angle / DEG_TO_RAD) * velocity;
            z_pos -= sin(view_angle / DEG_TO_RAD) * velocity;
            moving = true;
        }
        
        if (directions & DIR_FORWARD) {
            x_pos -= sin(view_angle / DEG_TO_RAD) * velocity;
            z_pos += cos(view_angle / DEG_TO_RAD) * velocity;
            moving = true;
        }
        
        if (directions & DIR_BACK) {
            x_pos += sin(view_angle / DEG_TO_RAD) * velocity;
            z_pos -= cos(view_angle / DEG_TO_RAD) * velocity;
            moving = true;
        }
        
        if (letters & LETTER_F) {
            if (fullscreen_toggle_done) {
                graphics_fullscreentoggle();
                is_fullscreen = !is_fullscreen;
            }
            fullscreen_toggle_done = false;
        } else {
            fullscreen_toggle_done = true;
        }
        
        /* scene process input end */
        
        /* scene perform routine */
        
        view_angle += (cursor_offset) / (float) current_width * MAX_DEGREES;
                
        if (fabs(view_angle) >= MAX_DEGREES)
            view_angle = 0.0f;
        
        object_angle += 1.0f;
        
        if (fabs(object_angle) >= MAX_DEGREES)
            object_angle = 0.0f;
        
        if (x_pos < -3.5f)
            x_pos = -3.5f;
        
        if (x_pos > 3.5f)
            x_pos = 3.5f;
        
        if (z_pos < -5.5f)
            z_pos = -5.5f;
        
        if (z_pos > 5.5f)
            z_pos = 5.5f;
        
        if (moving) {
            hand_x_phase += 5.0f * (velocity / 0.1f);
            hand_y_phase += 10.0f * (velocity / 0.1f);
        }
        
        if (fabs(hand_x_phase) > MAX_DEGREES)
            hand_x_phase = 0.0f;
        
        if (fabs(hand_y_phase) >= MAX_DEGREES / 2.0f)
            hand_y_phase = -hand_y_phase;
        
        hand_pos_x = (float) fmod(sin(hand_x_phase / DEG_TO_RAD), M_PI) * 0.2f;
        hand_pos_y = (float) fmod(cos(hand_y_phase / DEG_TO_RAD), M_PI) * 0.1f - 0.2f;
        
        /* scene perform routine end */
        
        /* scene draw */
        
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glFrustum(-1.0, 1.0, -1.0, 1.0, 0.5, 10.0);
        
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        glPushMatrix();
            glRotatef(view_angle, 0.0f, 1.0f, 0.0f);
            glTranslatef(x_pos, y_pos, z_pos);
        
            for (i = floor_start; i < floor_depth; i++) {
                draw_tile(-10.0f, floor_height, (float) -i);
                draw_tile(-8.0f, floor_height, (float) -i);
                draw_tile(-6.0f, floor_height, (float) -i);
                draw_tile(-4.0f, floor_height, (float) -i);
                draw_tile(-2.0f, floor_height, (float) -i);
                draw_tile(0.0f, floor_height, (float) -i);
                draw_tile(2.0f, floor_height, (float) -i);
                draw_tile(4.0f, floor_height, (float) -i);
                draw_tile(6.0f, floor_height, (float) -i);
                draw_tile(8.0f, floor_height, (float) -i);
                draw_tile(10.0f, floor_height, (float) -i);
            }
        
            glPushMatrix();
                glRotatef(90.0f, 0.0f, 0.0f, 1.0f);
                for (i = floor_start; i < floor_depth; i++) {
                    draw_tile(-10.0f, floor_height * 2.0f, (float) -i);
                    draw_tile(-8.0f, floor_height * 2.0f, (float) -i);
                    draw_tile(-6.0f, floor_height * 2.0f, (float) -i);
                    draw_tile(-4.0f, floor_height * 2.0f, (float) -i);
                    draw_tile(-2.0f, floor_height * 2.0f, (float) -i);
                    draw_tile(0.0f, floor_height * 2.0f, (float) -i);
                    draw_tile(2.0f, floor_height * 2.0f, (float) -i);
                    draw_tile(4.0f, floor_height * 2.0f, (float) -i);
                    draw_tile(6.0f, floor_height * 2.0f, (float) -i);
                    draw_tile(8.0f, floor_height * 2.0f, (float) -i);
                    draw_tile(10.0f, floor_height * 2.0f, (float) -i);
                }
            glPopMatrix();
        
            glPushMatrix();
                glRotatef(-90.0f, 0.0f, 0.0f, 1.0f);
                for (i = floor_start; i < floor_depth; i++) {
                    draw_tile(-10.0f, floor_height * 2.0f, (float) -i);
                    draw_tile(-8.0f, floor_height * 2.0f, (float) -i);
                    draw_tile(-6.0f, floor_height * 2.0f, (float) -i);
                    draw_tile(-4.0f, floor_height * 2.0f, (float) -i);
                    draw_tile(-2.0f, floor_height * 2.0f, (float) -i);
                    draw_tile(0.0f, floor_height * 2.0f, (float) -i);
                    draw_tile(2.0f, floor_height * 2.0f, (float) -i);
                    draw_tile(4.0f, floor_height * 2.0f, (float) -i);
                    draw_tile(6.0f, floor_height * 2.0f, (float) -i);
                    draw_tile(8.0f, floor_height * 2.0f, (float) -i);
                    draw_tile(10.0f, floor_height * 2.0f, (float) -i);
                }
            glPopMatrix();
        
            glPushMatrix();
                glRotatef(180.0f, 0.0f, 0.0f, 1.0f);
                for (i = floor_start; i < floor_depth; i++) {
                    draw_tile(-10.0f, floor_height * 3.0f, (float) -i);
                    draw_tile(-8.0f, floor_height * 3.0f, (float) -i);
                    draw_tile(-6.0f, floor_height * 3.0f, (float) -i);
                    draw_tile(-4.0f, floor_height * 3.0f, (float) -i);
                    draw_tile(-2.0f, floor_height * 3.0f, (float) -i);
                    draw_tile(0.0f, floor_height * 3.0f, (float) -i);
                    draw_tile(2.0f, floor_height * 3.0f, (float) -i);
                    draw_tile(4.0f, floor_height * 3.0f, (float) -i);
                    draw_tile(6.0f, floor_height * 3.0f, (float) -i);
                    draw_tile(8.0f, floor_height * 3.0f, (float) -i);
                    draw_tile(10.0f, floor_height * 3.0f, (float) -i);
                }
            glPopMatrix();
        
            glPushMatrix();
                glTranslatef(0.0f, 0.7f, -3.0f);
                glRotatef(object_angle, 0.0f, 1.0f, 0.0f);
                glBegin(GL_TRIANGLES);
                    glColor3f(0.0f, 0.0f, 1.0f);
                    glVertex3f(0.0f, 0.5f, 0.0f);
                    glVertex3f(0.5f, -0.5f, 0.3f);
                    glVertex3f(-0.5f, -0.5f, 0.3f);
            
                    glColor3f(1.0f, 0.0f, 0.0f);
                    glVertex3f(0.0f, 0.5f, 0.0f);
                    glVertex3f(0.0f, -0.5f, -0.6f);
                    glVertex3f(0.5f, -0.5f, 0.3f);
            
                    glColor3f(0.0f, 1.0f, 0.0f);
                    glVertex3f(0.0f, 0.5f, 0.0f);
                    glVertex3f(-0.5f, -0.5f, 0.3f);
                    glVertex3f(0.0f, -0.5f, -0.6f);
            
                    glColor3f(1.0f, 1.0f, 1.0f);
                    glVertex3f(0.0f, -0.5f, -0.6f);
                    glVertex3f(-0.5f, -0.5f, 0.3f);
                    glVertex3f(0.5f, -0.5f, 0.3f);
                glEnd();
            glPopMatrix();
        glPopMatrix();
        
        glFlush();
        
        glMatrixMode(GL_PROJECTION);
            glPushMatrix();
                glLoadIdentity();
                glOrtho(-1.0, 1.0, -1.0, 1.0, -1.0, 1.0);
                
                glMatrixMode(GL_MODELVIEW);
                glLoadIdentity();
        
                glTranslatef(hand_pos_x, hand_pos_y, 0.0f);
                
                glBegin(GL_QUADS);
                    glColor3f(0.2f, 0.2f, 0.2f);
                    glVertex3f(-0.10f, -0.2f, 0.0f);
                    glVertex3f(0.10f, -0.2f, 0.0f);
                    glColor3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(0.15f, -1.0f, 0.0f);
                    glVertex3f(-0.15f, -1.0f, 0.0f);
                glEnd();
            glPopMatrix();
        
        /* scene draw end */
        
        /* cycle subsystems */

        graphics_flip();
        
        input_poll();
        
        /* cycle subsystems end */
        
        /* reset state */
        
        moving = false;
        
        engine_state.is_running = graphics_running();
        
        /* reset state end */
        
	}

    graphics_free();

	return EXIT_SUCCESS;
}
