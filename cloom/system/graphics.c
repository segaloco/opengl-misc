#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef __APPLE__
#define GL_SILENCE_DEPRECATION
#endif /* __APPLE__ */
#include <GLFW/glfw3.h>

#include "graphics.h"

extern void input_init(void);
extern void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods);

GLFWwindow *window;
static int _window_width, _window_height;
static double _cursor_x, _cursor_y;
static bool _is_fullscreen;
static int _window_pos_x, _window_pos_y;
int current_width;

static void error_callback(int error, const char *description)
{
    fputs(description, stderr);
}

static void framebuffer_size_callback(GLFWwindow *window, int width, int height)
{
    glViewport(0, 0, width, height);
}

void graphics_init(int window_width, int window_height, const char *title, bool is_fullscreen)
{
    _window_width = window_width;
    _window_height = window_height;
    
    if (!glfwInit())
        exit(EXIT_FAILURE);
    
    glfwSetErrorCallback(error_callback);
    
    if (!(window = glfwCreateWindow(_window_width, _window_height, title, NULL, NULL))) {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }
    
    current_width = _window_width;
    
    glfwMakeContextCurrent(window);
    glfwSetKeyCallback(window, key_callback);
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    if (glfwRawMouseMotionSupported())
        glfwSetInputMode(window, GLFW_RAW_MOUSE_MOTION, GLFW_TRUE);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    
    if (is_fullscreen)
        graphics_fullscreentoggle();
}

void graphics_free(void)
{
    glfwDestroyWindow(window);
    glfwTerminate();
}

bool graphics_running(void)
{
    return !glfwWindowShouldClose(window);
}

void graphics_flip(void)
{
    glfwSwapBuffers(window);
}

void graphics_fullscreentoggle(void)
{
    if (!_is_fullscreen) {
        GLFWmonitor *current_monitor;
        const GLFWvidmode *monitor_mode;
        
        glfwGetWindowPos(window, &_window_pos_x, &_window_pos_y);
        glfwGetWindowSize(window, &_window_width, &_window_height);
        
        current_monitor = glfwGetPrimaryMonitor();
        
        monitor_mode = glfwGetVideoMode(current_monitor);
        
        glfwSetWindowMonitor(window, current_monitor, 0, 0, monitor_mode->width, monitor_mode->height, 0);
        
        current_width = monitor_mode->width;
    } else {
        glfwSetWindowMonitor(window, NULL, _window_pos_x, _window_pos_y, _window_width, _window_height, 0);
        current_width = _window_width;
    }
    
    _is_fullscreen = !_is_fullscreen;
    input_init();
}
