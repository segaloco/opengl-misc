#ifndef INPUT_H
#define INPUT_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stdbool.h>

enum direction_keys {
    DIR_FORWARD = 1
    ,DIR_BACK = 2
    ,DIR_LEFT = 4
    ,DIR_RIGHT = 8
};

enum letter_keys {
    LETTER_A = 1
    ,LETTER_B = 2
    ,LETTER_C = 3
    ,LETTER_D = 4
    ,LETTER_E = 5
    ,LETTER_F = 6
};

extern char directions;
extern long letters;
extern long cursor_offset;
extern bool shift_held;

extern void input_init(void);
extern void input_poll(void);

#ifdef __cplusplus
};
#endif /* __cplusplus */

#endif /* INPUT_H */
