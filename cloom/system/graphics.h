#ifndef GRAPHICS_H
#define GRAPHICS_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stdbool.h>

extern int current_width;

extern void graphics_init(int window_width, int window_height, const char *title, bool is_fullscreen);
extern void graphics_free(void);
extern bool graphics_running(void);
extern void graphics_flip(void);
extern void graphics_fullscreentoggle(void);

#ifdef __cplusplus
};
#endif /* __cplusplus */

#endif /* GRAPHICS_H */
