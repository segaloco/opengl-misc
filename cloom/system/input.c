#include <stdbool.h>

#ifdef __APPLE__
#define GL_SILENCE_DEPRECATION
#endif /* __APPLE__ */
#include <GLFW/glfw3.h>

#include "input.h"

extern GLFWwindow *window;

static double cursor_x, cursor_y, prev_cursor_x, prev_cursor_y;
static bool left_shift = false, right_shift = false;

char directions = 0;
long letters = 0;
long cursor_offset = 0;
bool shift_held = false;

void key_callback(GLFWwindow *GLwindow, int key, int scancode, int action, int mods)
{
    if (action == GLFW_PRESS || action == GLFW_REPEAT) {
        switch (key) {
        case GLFW_KEY_ESCAPE:
            glfwSetWindowShouldClose(GLwindow, GL_TRUE);
            break;
        case GLFW_KEY_A:
            directions |= DIR_LEFT;
            break;
        case GLFW_KEY_D:
            directions |= DIR_RIGHT;
            break;
        case GLFW_KEY_W:
            directions |= DIR_FORWARD;
            break;
        case GLFW_KEY_S:
            directions |= DIR_BACK;
            break;
        case GLFW_KEY_F:
            if (action == GLFW_PRESS)
                letters |= LETTER_F;
            break;
        case GLFW_KEY_LEFT_SHIFT:
            left_shift = true;
            break;
        case GLFW_KEY_RIGHT_SHIFT:
            right_shift = true;
            break;
        default:
            break;
        }
    } else if (action == GLFW_RELEASE) {
        switch (key) {
        case GLFW_KEY_A:
            directions &= ~DIR_LEFT;
            break;
        case GLFW_KEY_D:
            directions &= ~DIR_RIGHT;
            break;
        case GLFW_KEY_W:
            directions &= ~DIR_FORWARD;
            break;
        case GLFW_KEY_S:
            directions &= ~DIR_BACK;
        case GLFW_KEY_F:
            letters &= ~LETTER_F;
            break;
        case GLFW_KEY_LEFT_SHIFT:
            left_shift = false;
            break;
        case GLFW_KEY_RIGHT_SHIFT:
            right_shift = false;
            break;
        default:
            break;
        }
    }
    
    shift_held = left_shift | right_shift;
}

void input_init(void)
{
    glfwGetCursorPos(window, &cursor_x, &cursor_y);
    
    prev_cursor_x = cursor_x;
    prev_cursor_y = cursor_y;
    cursor_offset = cursor_x - prev_cursor_x;
}

void input_poll(void)
{
    prev_cursor_x = cursor_x;
    prev_cursor_y = cursor_y;
    
    glfwGetCursorPos(window, &cursor_x, &cursor_y);
    cursor_offset = cursor_x - prev_cursor_x;
    
    glfwPollEvents();
}
