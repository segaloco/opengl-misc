#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <Windows.h>
#include <gl/GL.h>

#define	WIN_RES_X	640
#define	WIN_RES_Y	480

HDC hDc;
int pixelFormat;
HGLRC hGlrc;

PROC wglSwapInterval;

bool engine_running = true;

uint8_t dir_keys;
#define DIR_LEFT	0x01
#define DIR_RIGHT	0x02
#define DIR_UP		0x04
#define DIR_DOWN	0x08

uint8_t num_keys;
#define KEY_1		0x01
#define	KEY_2		0x02
#define	KEY_3		0x04
#define	KEY_4		0x08

uint8_t sys_keys;
#define	TOGGLE_FULLSCREEN	0x01

GLfloat tri_x, tri_y, tri_z;
GLfloat bg_x, bg_y, bg_z;

GLfloat velocity_factor = 0.03f;
GLfloat player_bounds = 0.6f;

bool is_setting_fullscreen = false;
bool is_fullscreen = false;

GLuint bgGridList;

bool is_setting_zoom_level = false;
float zoom_level = 1.0f;

static void setWindowed(HWND hWnd)
{
	RECT client_rect;

	client_rect.top = 0;
	client_rect.bottom = WIN_RES_Y;
	client_rect.left = 0;
	client_rect.right = WIN_RES_X;

	ShowWindow(hWnd, SW_HIDE);
	wglMakeCurrent(NULL, NULL);
	ShowCursor(TRUE);
	SetWindowLongPtr(hWnd, GWL_STYLE, WS_OVERLAPPEDWINDOW);
	AdjustWindowRect(&client_rect, WS_OVERLAPPEDWINDOW, FALSE);
	SetWindowPos(hWnd, HWND_TOP, CW_USEDEFAULT, CW_USEDEFAULT, WIN_RES_X, WIN_RES_Y, SWP_NOMOVE);
	wglMakeCurrent(hDc, hGlrc);
	glViewport(0, 0, WIN_RES_X, WIN_RES_Y);
	ShowWindow(hWnd, SW_SHOW);
}

static void setFullscreen(HWND hWnd)
{
	int xres, yres;

	xres = GetSystemMetrics(SM_CXSCREEN);
	yres = GetSystemMetrics(SM_CYSCREEN);

	ShowWindow(hWnd, SW_HIDE);
	wglMakeCurrent(NULL, NULL);
	ShowCursor(FALSE);
	SetWindowLongPtr(hWnd, GWL_STYLE, WS_POPUP | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);
	SetWindowPos(hWnd, HWND_TOP, 0, 0, xres, yres, 0);
	wglMakeCurrent(hDc, hGlrc);
	glViewport(0, 0, xres, yres);
	ShowWindow(hWnd, SW_SHOW);
	SetForegroundWindow(hWnd);
	SetFocus(hWnd);
}

void drawBgGrid()
{
	glBegin(GL_QUADS);
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex3f(-1.0f, 1.0f, -2.0f);
		glVertex3f(0.0f, 1.0f, -2.0f);
		glVertex3f(0.0f, 0.0f, -2.0f);
		glVertex3f(-1.0f, 0.0f, -2.0f);
	glEnd();
	glBegin(GL_QUADS);
		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(0.0f, 1.0f, -2.0f);
		glVertex3f(1.0f, 1.0f, -2.0f);
		glVertex3f(1.0f, 0.0f, -2.0f);
		glVertex3f(0.0f, 0.0f, -2.0f);
	glEnd();
	glBegin(GL_QUADS);
		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(-1.0f, 0.0f, -2.0f);
		glVertex3f(0.0f, 0.0f, -2.0f);
		glVertex3f(0.0f, -1.0f, -2.0f);
		glVertex3f(-1.0f, -1.0f, -2.0f);
	glEnd();
	glBegin(GL_QUADS);
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 0.0f, -2.0f);
		glVertex3f(1.0f, 0.0f, -2.0f);
		glVertex3f(1.0f, -1.0f, -2.0f);
		glVertex3f(0.0f, -1.0f, -2.0f);
	glEnd();

	return;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	PIXELFORMATDESCRIPTOR pfd = { 0 };
	RECT client_rect;
	bool frame_centered_x = false;
	bool frame_centered_y = false;

	pfd.nSize = sizeof (PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER | PFD_SWAP_LAYER_BUFFERS;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 24;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 24;
	pfd.cStencilBits = 8;

	switch (uMsg) {
	case WM_CREATE:
		hDc = GetDC(hWnd);
		pixelFormat = ChoosePixelFormat(hDc, &pfd);
		SetPixelFormat(hDc, pixelFormat, &pfd);

		hGlrc = wglCreateContext(hDc);
		wglMakeCurrent(hDc, hGlrc);

		wglSwapInterval = wglGetProcAddress("wglSwapIntervalEXT");

		wglSwapInterval(1);

		glEnable(GL_DEPTH_TEST);
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

		bgGridList = glGenLists(1);
		glNewList(bgGridList, GL_COMPILE);
			drawBgGrid();
		glEndList();
		break;
	case WM_CLOSE:
		wglMakeCurrent(NULL, NULL);
		wglDeleteContext(hGlrc);
		ReleaseDC(hWnd, hDc);
		DestroyWindow(hWnd);
		break;
	case WM_DESTROY:
		engine_running = false;
		PostQuitMessage(0);
		break;
	case WM_SIZE:
		GetClientRect(hWnd, &client_rect);
		glViewport(0, 0, (GLsizei) (client_rect.right - client_rect.left), (GLsizei) (client_rect.bottom - client_rect.top));
		break;
	case WM_SYSKEYDOWN:
		switch (wParam) {
		case VK_RETURN:
			sys_keys |= TOGGLE_FULLSCREEN;
			break;
		default:
			break;
		}
		break;
	case WM_SYSKEYUP:
		switch (wParam) {
		case VK_RETURN:
			sys_keys &= ~TOGGLE_FULLSCREEN;
			break;
		default:
			break;
		}
		break;
	case WM_KEYDOWN:
		switch (wParam) {
		case VK_ESCAPE:
			PostMessage(hWnd, WM_CLOSE, 0, 0);
			break;
		case 'W':
		case 'w':
			dir_keys |= DIR_UP;
			break;
		case 'S':
		case 's':
			dir_keys |= DIR_DOWN;
			break;
		case 'A':
		case 'a':
			dir_keys |= DIR_LEFT;
			break;
		case 'D':
		case 'd':
			dir_keys |= DIR_RIGHT;
			break;
		case '1':
			num_keys |= KEY_1;
			break;
		case '2':
			num_keys |= KEY_2;
			break;
		case '3':
			num_keys |= KEY_3;
			break;
		case '4':
			num_keys |= KEY_4;
			break;
		default:
			break;
		}
		break;
	case WM_KEYUP:
		switch (wParam) {
		case 'W':
		case 'w':
			dir_keys &= ~DIR_UP;
			break;
		case 'S':
		case 's':
			dir_keys &= ~DIR_DOWN;
			break;
		case 'A':
		case 'a':
			dir_keys &= ~DIR_LEFT;
			break;
		case 'D':
		case 'd':
			dir_keys &= ~DIR_RIGHT;
			break;
		case '1':
			num_keys &= ~KEY_1;
			break;
		case '2':
			num_keys &= ~KEY_2;
			break;
		case '3':
			num_keys &= ~KEY_3;
			break;
		case '4':
			num_keys &= ~KEY_4;
			break;
		default:
			break;
		}
		break;
	case WM_PAINT:
		if (dir_keys & DIR_LEFT) {
			tri_x -= velocity_factor;
		}
		if (dir_keys & DIR_RIGHT) {
			tri_x += velocity_factor;
		}
		if (dir_keys & DIR_UP) {
			tri_y += velocity_factor;
		}
		if (dir_keys & DIR_DOWN) {
			tri_y -= velocity_factor;
		}

		if (tri_x > player_bounds) {
			tri_x = player_bounds;
			bg_x -= velocity_factor;
		} else if (tri_x < -player_bounds) {
			tri_x = -player_bounds;
			bg_x += velocity_factor;
		}

		if (tri_y > player_bounds) {
			tri_y = player_bounds;
			bg_y -= velocity_factor;
		} else if (tri_y < -player_bounds) {
			tri_y = -player_bounds;
			bg_y += velocity_factor;
		}

		if (bg_x > 2.0f) {
			bg_x -= 2.0f;
		} else if (bg_x < -2.0f) {
			bg_x += 2.0f;
		}

		if (bg_y > 2.0f) {
			bg_y -= 2.0f;
		} else if (bg_y < -2.0f) {
			bg_y += 2.0f;
		}

		if (sys_keys & TOGGLE_FULLSCREEN) {
			if (!is_setting_fullscreen) {
				is_setting_fullscreen = true;

				if (is_fullscreen) {
					setWindowed(hWnd);
					is_fullscreen = false;
				} else {
					setFullscreen(hWnd);
					is_fullscreen = true;
				}
			}
		} else {
			is_setting_fullscreen = false;
		}

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();

		if (num_keys != 0) {
			if (!is_setting_zoom_level) {
				is_setting_zoom_level = true;
				if (num_keys & KEY_1)
					zoom_level = 1.0f;
				else if (num_keys & KEY_2)
					zoom_level = 2.0f;
				else if (num_keys & KEY_3)
					zoom_level = 3.0f;
				else if (num_keys & KEY_4)
					zoom_level = 4.0f;
			}
		} else {
			is_setting_zoom_level = false;
		}

		glOrtho(-zoom_level, zoom_level, -zoom_level, zoom_level, -3.0, 3.0);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		glPushMatrix();
			glTranslatef(bg_x, bg_y, bg_z);
			glCallList(bgGridList);

			if (bg_x == 0.0f)
				frame_centered_x = true;
			if (bg_y == 0.0f)
				frame_centered_y = true;

			if (!frame_centered_x || !frame_centered_y) {

				/* handle horizontal tile drawing */
				if (!frame_centered_x) {
					glPushMatrix();
						if (bg_x > 0.0f)
							glTranslatef(-2.0f, 0.0f, 0.0f);
						else if (bg_x < 0.0f)
							glTranslatef(2.0f, 0.0f, 0.0f);

						glCallList(bgGridList);
					glPopMatrix();
				}

				/* handle vertical tile drawing */
				if (!frame_centered_y) {
					glPushMatrix();
						if (bg_y > 0.0f)
							glTranslatef(0.0f, -2.0f, 0.0f);
						else if (bg_y < 0.0f)
							glTranslated(0.0f, 2.0f, 0.0f);

						glCallList(bgGridList);
					glPopMatrix();
				}
				
				/* handle diagonal tile drawing */
				if (!frame_centered_x && !frame_centered_y)
				glPushMatrix();
					if (bg_x > 0.0f && bg_y > 0.0f)
						glTranslatef(-2.0f, -2.0f, 0.0f);
					else if (bg_x > 0.0f && bg_y < 0.0f)
						glTranslatef(-2.0f, 2.0f, 0.0f);
					else if (bg_x < 0.0f && bg_y > 0.0f)
						glTranslatef(2.0f, -2.0f, 0.0f);
					else if (bg_x < 0.0f && bg_y < 0.0f)
						glTranslatef(2.0f, 2.0f, 0.0f);

					glCallList(bgGridList);
				glPopMatrix();
			}
		glPopMatrix();

		glPushMatrix();
			glTranslatef(tri_x, tri_y, tri_z);
			glBegin(GL_TRIANGLES);
				glColor3f(0.0f, 1.0f, 0.0f);
				glVertex3f(0.0f, 0.25f, 0.0f);
				glVertex3f(0.25f, -0.25f, 0.0f);
				glVertex3f(-0.25f, -0.25f, 0.0f);
			glEnd();
		glPopMatrix();

		glFlush();

		wglSwapLayerBuffers(hDc, WGL_SWAP_MAIN_PLANE);
		break;
	default:
		return DefWindowProc(hWnd, uMsg, wParam, lParam);
	}

	return 0;
}

int main(int argc, char* argv[])
{
	HINSTANCE hInstance;
	LPCTSTR wndClassName = L"WndClass";
	WNDCLASS wndClass = { 0 };
	HWND hWnd;
	MSG msg;

	hInstance = GetModuleHandle(NULL);

	wndClass.style = CS_OWNDC | CS_VREDRAW | CS_HREDRAW;
	wndClass.lpfnWndProc = WndProc;
	wndClass.hInstance = hInstance;
	wndClass.hbrBackground = (HBRUSH) COLOR_BACKGROUND;
	wndClass.lpszClassName = wndClassName;

	RegisterClass(&wndClass);

	hWnd = CreateWindow(
		wndClassName,
		L"OpenGL",
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		WIN_RES_X,
		WIN_RES_Y,
		NULL,
		NULL,
		hInstance,
		NULL
	);

	ShowWindow(hWnd, SW_SHOW);

	tri_x = 0.0f;
	tri_y = 0.0f;
	tri_z = 0.0f;
	bg_x = 0.0f;
	bg_y = 0.0f;
	bg_z = 0.0f;
	dir_keys = 0;
	sys_keys = 0;

	while (engine_running) {
		while (GetMessage(&msg, NULL, 0, 0) != 0) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	UnregisterClass(wndClassName, hInstance);

	return EXIT_SUCCESS;
}