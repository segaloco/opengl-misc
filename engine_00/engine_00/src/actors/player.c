#include <stdbool.h>

#include "../system/system.h"

#include "../scene/collision.h"
#include "../scene/node.h"
#include "../scene/scene.h"

#include "actors.h"

static bool doing_jump = false;
static void move(input_state_t *input_state, node_t *player_node, const node_t *camera_node);
static void collide(node_t *this, node_t **tree, unsigned int *node_count, collision_t **queue, unsigned int *queue_size);
static void camera_calc(node_t *camera_node, const node_t *player_node);

void player_routine(node_t *this, node_t **tree, collision_t **col_queue, input_state_t *input_state, unsigned int *tree_size, unsigned int *col_queue_size, unsigned int camera_node_index, unsigned int player_node_index)
{
    node_t *camera_node;

    camera_node = &(*tree)[camera_node_index];

    move(input_state, this, camera_node);
    collide(this, tree, tree_size, col_queue, col_queue_size);
    camera_calc(camera_node, this);
}

/* perform movement of the character with respect to input and viewing angle */
static void move(input_state_t *input_state, node_t *this, const node_t *camera_node)
{
    physics_props_t *physics_props;
    player_props_t *player_props;
    coord_t camera_angle;
    coord_t *translate;
    coord_t *velocity;
    coord_t *acceleration;
    bool moving_left = false;
    bool moving_right = false;
    bool moving_forward = false;
    bool moving_backward = false;
    bool do_jump = false;
    
    physics_props = &this->physics_props;
    player_props = (player_props_t *) this->properties_ext;

    vector_components(1.0, camera_node->rotate.axis.x, &camera_angle);

    translate = &this->translate;
    velocity = &physics_props->velocity;
    acceleration = &physics_props->acceleration;
    
    physics_props->last_position = *translate;

    acceleration->x = 0.0;
    acceleration->y = 0.0;
    acceleration->z = 0.0;

    moving_forward = input_state->letter_keys & LETTER_KEY_W;
    moving_backward = input_state->letter_keys & LETTER_KEY_S;
    moving_left = input_state->letter_keys & LETTER_KEY_A;
    moving_right = input_state->letter_keys & LETTER_KEY_D;
    do_jump = input_state->spec_keys & SPEC_KEY_SPACE;

    if (moving_forward) {
        acceleration->x += player_props->movement_factor * camera_angle.y;
        acceleration->z += player_props->movement_factor * camera_angle.x;
    }
    if (moving_backward) {
        acceleration->x -= player_props->movement_factor * camera_angle.y;
        acceleration->z -= player_props->movement_factor * camera_angle.x;
    }
    if (moving_left) {
        vector_components(-player_props->movement_factor, camera_node->rotate.axis.x, &camera_angle);

        acceleration->x += camera_angle.x;
        acceleration->z -= camera_angle.y;
    }
    if (moving_right) {
        vector_components(player_props->movement_factor, camera_node->rotate.axis.x, &camera_angle);

        acceleration->x += camera_angle.x;
        acceleration->z -= camera_angle.y;
    }
    if (
        do_jump
        && (player_props->flags & PLAYER_FLAG_FLOOR)
        && !(player_props->flags & PLAYER_FLAG_JUMPING)
    ) {
        acceleration->y = player_props->jump_factor;
        player_props->flags |= PLAYER_FLAG_JUMPING;
        player_props->flags &= ~PLAYER_FLAG_FLOOR;
    }

    *translate = physics_apply_motion(physics_props, player_props->flags & PLAYER_FLAG_FLOOR);
}

/* perform priority collision for player placement */
static void collide(node_t *this, node_t **tree, unsigned int *node_count, collision_t **queue, unsigned int *queue_size)
{
    unsigned int i;
    physics_props_t *physics_props;
    player_props_t *player_props;
    coord_t *position;
    coord_t *velocity;
    coord_t *acceleration;
    bool x_bound, y_bound, z_bound;

    x_bound = y_bound = z_bound = false;

    physics_props = &this->physics_props;
    position = &this->translate;
    velocity = &physics_props->velocity;
    acceleration = &physics_props->acceleration;
    player_props = (player_props_t *) this->properties_ext;

    if (position->y <= 0.0) {
        velocity->y = 0.0;
        position->y = 0.0;
        player_props->flags |= PLAYER_FLAG_FLOOR;
    }

    if (position->x < -20.0) {
        position->x = -20.0;
        x_bound = true;
    }
    else if (position->x > 20.0) {
        position->x = 20.0;
        x_bound = true;
    }

    if (x_bound) {
        velocity->x = 0.0;
        acceleration->x = 0.0;
    }

    if (position->z < -20.0) {
        position->z = -20.0;
        z_bound = true;
    }
    else if (position->z > 20.0) {
        position->z = 20.0;
        z_bound = true;
    }

    if (z_bound) {
        velocity->z = 0.0;
        acceleration->z = 0.0;
    }

    if (!this->is_active)
        return;
    
    if (this->collision_body.type == COLLISION_BODY_TYPE_NONE)
        return;
    
    for (i = 0; i < *node_count; i++) {
        node_t *node = &(*tree)[i];
        bool from_above = false;

        if (!node->is_active)
            continue;
        
        if (node == this)
            continue;
        
        if (this->collision_body.type == COLLISION_BODY_TYPE_BOX) {
            collision_box_t *collision_box = (collision_box_t *) this->collision_body.geometry;
            
            collision_box->position = *position;
        }
        
        if (!collision_determine(&this->collision_body, &node->collision_body))
            continue;

        if (node->collision_body.reactive)
            collision_register(&this->collision_body, &node->collision_body, queue, queue_size);
        
        if (!this->collision_body.reactive)
            continue;

        switch (node->id) {
        case 4:
        case 5:
        case 6:
            /* if a box, return to last point before collision happened */
            /* if landing on top, set proper flag */
            /* if hitting a ceiling, set proper flag */
            *position = this->physics_props.last_position;

            if (this->physics_props.last_position.y > (node->translate.y + 1.0))
                player_props->flags |= PLAYER_FLAG_FLOOR;
            break;
        default:
            break;
        }
    }

    if (player_props->flags & PLAYER_FLAG_FLOOR)
        player_props->flags &= ~PLAYER_FLAG_JUMPING;
}

/* perform calculation of the camera against player position */
static void camera_calc(node_t *camera_node, const node_t *player_node)
{
    camera_props_t *camera_props;
    coord_t camera_dist;
    double camera_magnitude;
    double camera_dist_z_adj;

    camera_props = (camera_props_t *) camera_node->properties_ext;

    camera_dist.x = player_node->translate.x - camera_node->translate.x;
    camera_dist.y = player_node->translate.y - camera_node->translate.y;
    camera_dist.z = player_node->translate.z - camera_node->translate.z;
    camera_dist_z_adj = sqrt(((camera_dist.x * camera_dist.x) + (camera_dist.z * camera_dist.z)));
    camera_magnitude = vector_dist(camera_node->translate, player_node->translate);

    camera_node->rotate.axis.x = vector_rotate(camera_dist.x, camera_dist.z);
    camera_node->rotate.axis.y = -vector_rotate(camera_dist.y, camera_dist_z_adj);

    if (camera_props->is_tracking) {
        if (camera_magnitude > camera_props->maximum_dist) {
            camera_node->translate.x += (camera_magnitude - camera_props->maximum_dist) * (camera_dist.x / camera_magnitude);
            camera_node->translate.z += (camera_magnitude - camera_props->maximum_dist) * (camera_dist.z / camera_magnitude);
        } else if (camera_magnitude < camera_props->minimum_dist) {
            camera_node->translate.x -= (camera_props->minimum_dist - camera_magnitude) * (camera_dist.x / camera_magnitude);
            camera_node->translate.z -= (camera_props->minimum_dist - camera_magnitude) * (camera_dist.z / camera_magnitude);
        }

        if (!doing_jump) {
            if (camera_node->translate.y > (camera_props->height_high_mark + player_node->translate.y))
                camera_node->translate.y -= camera_props->height_factor;
            else if (camera_node->translate.y < (camera_props->height_low_mark + player_node->translate.y))
                camera_node->translate.y += camera_props->height_factor;
        }
    }

    camera_node->rotate.axis.x = fmod(camera_node->rotate.axis.x, 360.0);
    camera_node->rotate.axis.y = fmod(camera_node->rotate.axis.y, 360.0);
}
