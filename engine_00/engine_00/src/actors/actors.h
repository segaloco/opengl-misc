#ifndef ACTORS_H
#define ACTORS_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "../system/system.h"

#include "../scene/node.h"
#include "../scene/scene.h"

/* camera node properties */
typedef struct camera_props {
    bool is_tracking;
    double minimum_dist;
    double maximum_dist;
    double height_high_mark;
    double height_low_mark;
    double height_factor;
} camera_props_t;

typedef enum player_flags {
    PLAYER_FLAG_FLOOR = 1,
    PLAYER_FLAG_CEILING = 2,
    PLAYER_FLAG_JUMPING = 4
} player_flags_t;

/* player node properties */
typedef struct player_props {
    double movement_factor;
    double jump_factor;
    player_flags_t flags;
} player_props_t;

extern void player_routine(node_t *this, node_t **tree, collision_t **col_queue, input_state_t *input_state, unsigned int *tree_size, unsigned int *col_queue_size, unsigned int camera_node_index, unsigned int player_node_index);
extern void coin_routine(node_t *this, node_t **tree, collision_t **col_queue, input_state_t *input_state, unsigned int *tree_size, unsigned int *col_queue_size, unsigned int camera_node_index, unsigned int player_node_index);

#ifdef __cplusplus
};
#endif /* __cplusplus */

#endif /* ACTORS_H */
