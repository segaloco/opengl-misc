#include <stdbool.h>

#include "../system/system.h"

#include "../scene/collision.h"
#include "../scene/node.h"
#include "../scene/scene.h"

static void collide(node_t *this, node_t **tree, unsigned int *node_count, collision_t **queue, unsigned int *queue_size, node_t *player_node);

void coin_routine(struct node *this, struct node **tree, collision_t **col_queue, input_state_t *input_state, unsigned int *tree_size, unsigned int *col_queue_size, unsigned int camera_node_index, unsigned int player_node_index)
{
    node_t *player_node;

    player_node = &(*tree)[player_node_index];

    collide(this, tree, tree_size, col_queue, col_queue_size, player_node);

    this->rotate.angle = fmod(this->rotate.angle + 2.0, 360.0);
}

static void collide(node_t *this, node_t **tree, unsigned int *node_count, collision_t **queue, unsigned int *queue_size, node_t *player_node)
{
    unsigned int i;

    if (this->collision_body.reactive) {
        for (i = 0; i < *queue_size; i++) {
            collision_t *collision = &(*queue)[i];

            if (!this->is_active)
                break;
            
            if (collision->processed)
                continue;
                
            if (collision->src == &player_node->collision_body && collision->dest == &this->collision_body) {
                this->is_active = false;
                this->is_display = false;
            }
        }
    }
}
