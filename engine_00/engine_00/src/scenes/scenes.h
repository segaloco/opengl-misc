#ifndef SCENES_H
#define SCENES_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "../scene/scene.h"

extern scene_t *scenes[];
extern unsigned int scene_count;

#ifdef __cplusplus
};
#endif /* __cplusplus */

#endif /* SCENES_H */
