#ifndef SCENES_INNER_H
#define SCENES_INNER_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "../scene/scene.h"

extern scene_t scene_00;

#ifdef __cplusplus
};
#endif /* __cplusplus */

#endif /* SCENES_INNER_H */
