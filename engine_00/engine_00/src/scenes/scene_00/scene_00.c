#include <stdbool.h>
#include <stddef.h>

#if defined(SYSTEM_GLX)
#include <GL/gl.h>
#elif defined(SYSTEM_CGL)
#include <OpenGL/gl.h>
#elif defined(SYSTEM_WGL)
#include <Windows.h>
#include <gl/GL.h>
#else
#error "The current system is not supported"
#endif /* SYSTEM_GLX | SYSTEM_CGL | SYSTEM_WGL */

#include "../../system/primitives.h"

#include "../../scene/mesh.h"
#include "../../scene/node.h"
#include "../../scene/scene.h"

#include "../../meshes/meshes.h"
#include "../../actors/actors.h"

collision_box_t collision_cube_1 = {
    { 0.0, 0.0, 0.0 },
    { 1.0, 1.0, 1.0 },
    2.0
};

collision_box_t collision_cube_2 = {
    { 0.0, 0.0, 0.0 },
    { 1.0, 1.0, 1.0 },
    2.0
};

collision_box_t collision_cube_3 = {
    { 0.0, 0.0, 0.0 },
    { 1.0, 1.0, 1.0 },
    2.0
};

collision_box_t collision_diamond_0 = {
    { 0.0, 0.0, 0.0 },
    { 0.2, 0.2, 0.2 },
    1.0
};

collision_box_t collision_diamond_1 = {
    { 0.0, 0.0, 0.0 },
    { 0.2, 0.2, 0.2 },
    1.0
};

collision_box_t collision_diamond_2 = {
    { 0.0, 0.0, 0.0 },
    { 0.2, 0.2, 0.2 },
    1.0
};

camera_props_t camera_props_ext = {
    true,   /* bool     is_tracking         */
    6.0,    /* double   minimum_dist        */
    8.0,    /* double   maximum_dist        */
    3.0,    /* double   height_high_mark    */
    1.5,    /* double   height_low_mark     */
    0.07    /* double   height_factor       */
};

collision_box_t collision_camera = {
    { 0.0, 0.0, 0.0 },
    { 1.0, 1.0, 1.0 },
    2.0
};

player_props_t player_props_ext = {
    0.02,   /* double           movement_factor     */
    0.26,   /* double           jump_factor         */
    0       /* player_flags_t   flags               */
};

collision_box_t collision_player = {
    { 0.0, 0.0, 0.0 },     /* coord_t  position    */
    { 1.0, 1.0, 1.0 },     /* coord_t  size        */
    2.0                    /* double   radius      */
};

node_t scene_nodes[] = {
    {
        1,
        &mesh_surface,
        { 1.0, 1.0, 1.0 },
        { 0.0, { 0.0, 0.0, 0.0 } },
        { 0.0, 0.0, 0.0 },
        0,
        false,
        true,
        { 0 },
        NULL,
        {
            COLLISION_BODY_TYPE_NONE,
            false,
            NULL,
            1
        },
        NULL
    },
    {
        2,                              /* unsigned int id              */    
        &mesh_player,                   /* mesh_t       *mesh           */
        { 1.0, 1.0, 1.0 },              /* coord_t      scale           */
        { 0.0, { 0.0, 0.0, 0.0 } },     /* rotate_t     rotate          */
        { 0.0, 0.0, -5.0 },             /* coord_t      translate       */
        0,                              /* GLuint       display_list    */
        true,                           /* bool         is_active       */
        true,                           /* bool         is_display      */
        {
            { 0.0, 0.0, 0.0 },      /* coord_t  velocity            */
            { 0.0, 0.0, 0.0 },      /* coord_t  acceleration        */
            0.01,                   /* double   friction_factor     */
            0.2,                    /* double   terminal_velocity   */
            -0.01,                  /* double   gravity_factor      */
            { 0.0, 0.0, 0.0 }       /* coord_t  last_position       */
        },
        &player_props_ext,              /* void         *properties     */
        {
            COLLISION_BODY_TYPE_BOX,    /* collision_body_type_t    type        */
            true,                       /* bool                     reactive    */
            &collision_player,          /* void                     *geometry   */
            2                           /* unsigned int             node_id     */
        },
        player_routine
    },
    {
        3,
        NULL,
        { 1.0, 1.0, 1.0 },
        { 0.0, { 0.0, 0.0, 0.0 } },
        { 10.0, 8.0, 10.0 },
        0,
        true,
        false,
        { 0 },
        &camera_props_ext,
        {
            COLLISION_BODY_TYPE_BOX,
            true,
            &collision_camera,
            3
        },
        NULL
    },
    {
        4,
        &mesh_cube,
        { 1.0, 1.0, 1.0 },
        { 0.0, { 0.0, 0.0, 0.0 } },
        { 3.0, 0.0, -7.0 },
        0,
        true,
        true,
        { 0 },
        NULL,
        {
            COLLISION_BODY_TYPE_BOX,
            false,
            &collision_cube_1,
            4
        },
        NULL
    },
    {
        5,
        &mesh_cube,
        {  1.0, 1.0, 1.0 },
        {  0.0, { 0.0, 0.0, 0.0 } },
        { -3.0, 0.0, -7.0 },
        0,
        true,
        true,
        { 0 },
        NULL,
        {
            COLLISION_BODY_TYPE_BOX,
            false,
            &collision_cube_2,
            5
        },
        NULL
    },
    {
        6,
        &mesh_cube,
        {  1.0, 1.0, 1.0 },
        {  0.0, { 0.0, 0.0, 0.0 } },
        {  0.0, 0.0, -9.0 },
        0,
        true,
        true,
        { 0 },
        NULL,
        {
            COLLISION_BODY_TYPE_BOX,
            false,
            &collision_cube_3,
            6
        },
        NULL
    },
    {
        7,
        &mesh_diamond,
        {  0.5, 0.5, 0.5 },
        {  0.0, { 0.0, 1.0, 0.0 } },
        {  0.0, 4.0, -9.0 },
        0,
        true,
        true,
        { 0 },
        NULL,
        {
            COLLISION_BODY_TYPE_BOX,
            true,
            &collision_diamond_0,
            7
        },
        coin_routine
    },
    {
        8,
        &mesh_diamond,
        {  0.5, 0.5, 0.5 },
        {  0.0, { 0.0, 1.0, 0.0 } },
        {  3.0, 4.0, -7.0 },
        0,
        true,
        true,
        { 0 },
        NULL,
        {
            COLLISION_BODY_TYPE_BOX,
            true,
            &collision_diamond_1,
            8
        },
        coin_routine
    },
    {
        9,
        &mesh_diamond,
        {  0.5, 0.5, 0.5 },
        {  0.0, { 0.0, 1.0, 0.0 } },
        { -3.0, 4.0, -7.0 },
        0,
        true,
        true,
        { 0 },
        NULL,
        {
            COLLISION_BODY_TYPE_BOX,
            true,
            &collision_diamond_2,
            9
        },
        coin_routine
    }
};

fog_props_t fog_props = {
    GL_EXP,             /* GLint mode   */
    0.30f, 1.0f, 10.0f  /* float density, start, end */
};

light_props_t lights_props[] = {
    {
        0,                      /* int      light       */
        { 0.0, 5.0,  0.0 },     /* coord_t  position    */
        { 0.0, -1.0, 0.0 },     /* coord_t  direction   */
    }
};

lighting_props_t lighting_props = {
    true,   /* bool color_material  */
    lights_props, sizeof (lights_props) / sizeof (light_props_t)    /* light_props_t *lights_props, size_t lights_count */
};

scene_t scene_00 = {
    scene_nodes, sizeof (scene_nodes) / sizeof (node_t),    /* node_t *nodes, size_t  node_count */
    1,                                                      /* size_t           player_node      */
    2,                                                      /* size_t           camera_node      */
    { 0.0, 0.2, 0.0, 0.0 },                                 /* rgba_t           clear_color      */
    { -0.1, 0.1, -0.1, 0.1, 0.1, 50.0 },                    /* projection_t     projection       */
    false,                                                  /* bool             is_ortho         */
    GFX_STATE_Z_BUFFER | GFX_STATE_BACK_CULL,               /* gfx_state_t      gfx_state        */
    { 0 },                                                  /* input_state_t    input_state      */
    true,                                                   /* bool             is_active        */
    &fog_props,                                             /* fog_props_t      *fog_props       */
    &lighting_props                                         /* lighting_props_t *lighting_props  */
};
