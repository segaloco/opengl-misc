#include <stdbool.h>

#include "../system/primitives.h"

#include "physics.h"

coord_t physics_apply_motion(physics_props_t *physics_props, bool on_ground)
{
    coord_t position, *last_position, *velocity, *acceleration;
    double friction, terminal_velocity;

    last_position = &physics_props->last_position;
    velocity = &physics_props->velocity;
    acceleration = &physics_props->acceleration;
    friction = physics_props->friction_factor;
    terminal_velocity = physics_props->terminal_velocity;
    
    if (acceleration->y == 0.0 && !on_ground)
        acceleration->y = physics_props->gravity_factor;

    velocity->x += acceleration->x;
    velocity->y += acceleration->y;
    velocity->z += acceleration->z;

    position.x = last_position->x + velocity->x;
    position.y = last_position->y + velocity->y;
    position.z = last_position->z + velocity->z;

    if (fabs(velocity->x) < friction)
        velocity->x = 0.0;
    else if (velocity->x > terminal_velocity)
        velocity->x = terminal_velocity;
    else if (velocity->x < -terminal_velocity)
        velocity->x = -terminal_velocity;
    else if (velocity->x > 0.0)
        velocity->x -= friction;
    else if (velocity->x < 0.0)
        velocity->x += friction;

    if (fabs(velocity->z) < friction)
        velocity->z = 0.0;
    else if (velocity->z > terminal_velocity)
        velocity->z = terminal_velocity;
    else if (velocity->z < -terminal_velocity)
        velocity->z = -terminal_velocity;
    else if (velocity->z > 0.0)
        velocity->z -= friction;
    else if (velocity->z < 0.0)
        velocity->z += friction;

    return position;
}
