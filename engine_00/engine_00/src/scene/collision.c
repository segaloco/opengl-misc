#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "../system/primitives.h"

#include "collision.h"

static bool box_on_box_determine(collision_body_t *body_a, collision_body_t *body_b);

bool collision_determine(collision_body_t *body_a, collision_body_t *body_b)
{
    switch (body_a->type) {
    case COLLISION_BODY_TYPE_BOX:
        switch (body_b->type) {
        case COLLISION_BODY_TYPE_BOX:
            return box_on_box_determine(body_a, body_b);
            break;
        default:
            break;
        }
        break;
    default:
        break;
    }
    
    return false;
}

void collision_register(collision_body_t *src, collision_body_t *dest, collision_t **collision_queue, unsigned int *collision_count)
{
    collision_t *collision;
    
    *collision_queue = realloc(*collision_queue, sizeof (**collision_queue) * ++(*collision_count));
    
    collision = collision_queue[*collision_count - 1];
    
    collision->src = src;
    collision->dest = dest;
    collision->processed = false;
}

static bool box_on_box_determine(collision_body_t *body_a, collision_body_t *body_b)
{
    collision_box_t *box_a, *box_b;
    coord_t box_a_max, box_a_min;
    coord_t box_b_max, box_b_min;
    
    box_a = (collision_box_t *) body_a->geometry;
    box_b = (collision_box_t *) body_b->geometry;
    
    box_a_max.x = box_a->position.x + box_a->size.x;
    box_a_min.x = box_a->position.x - box_a->size.x;
    box_a_max.y = box_a->position.y + box_a->size.y;
    box_a_min.y = box_a->position.y - box_a->size.y;
    box_a_max.z = box_a->position.z + box_a->size.z;
    box_a_min.z = box_a->position.z - box_a->size.z;
    
    box_b_max.x = box_b->position.x + box_b->size.x;
    box_b_min.x = box_b->position.x - box_b->size.x;
    box_b_max.y = box_b->position.y + box_b->size.y;
    box_b_min.y = box_b->position.y - box_b->size.y;
    box_b_max.z = box_b->position.z + box_b->size.z;
    box_b_min.z = box_b->position.z - box_b->size.z;
    
    if (
        (box_a_max.x > box_b_min.x && box_a_min.x < box_b_max.x)
        && (box_a_max.y > box_b_min.y && box_a_min.y < box_b_max.y)
        && (box_a_max.z > box_b_min.z && box_a_min.z < box_b_max.z)
    ) {
        return true;
    } else {
        return false;
    }
}
