#include <stddef.h>

#if defined(SYSTEM_GLX)
#include <GL/gl.h>
#elif defined(SYSTEM_CGL)
#include <OpenGL/gl.h>
#elif defined(SYSTEM_WGL)
#include <Windows.h>
#include <gl/GL.h>
#else
#error "The current system is not supported"
#endif /* SYSTEM_GLX | SYSTEM_CGL | SYSTEM_WGL */

#include "mesh.h"

void mesh_draw(mesh_t *this)
{
    unsigned int i;

    glBegin(this->mode);

        for (i = 0; i < this->vertex_count; i++) {
            if (this->normals != NULL)
                glNormal3d((GLdouble) this->normals[i].x, (GLdouble) this->normals[i].y, (GLdouble) this->normals[i].z);
            if (this->colors != NULL)
                glColor4d((GLdouble) this->colors[i].red, (GLdouble) this->colors[i].green, (GLdouble) this->colors[i].blue, (GLdouble) this->colors[i].alpha);
            glVertex3d((GLdouble) this->vertices[i].x, (GLdouble) this->vertices[i].y, (GLdouble) this->vertices[i].z);
        }

    glEnd();
}
