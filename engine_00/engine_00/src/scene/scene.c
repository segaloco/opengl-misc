#include <math.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#if defined(SYSTEM_GLX)
#include <GL/gl.h>
#elif defined(SYSTEM_CGL)
#include <OpenGL/gl.h>
#elif defined(SYSTEM_WGL)
#include <Windows.h>
#include <gl/GL.h>
#else
#error "The current system is not supported"
#endif /* SYSTEM_GLX | SYSTEM_CGL | SYSTEM_WGL */

#include "../system/system.h"
#include "../system/primitives.h"

#include "node.h"
#include "scene.h"

static bool toggling_fs = false;

static void scene_apply_gfx_state(scene_t *this);

static void scene_project(const scene_t *this);

void scene_init(scene_t *this)
{
    unsigned int i;
    
    this->is_active = true;
    this->collision_queue = NULL;
    this->collision_count = 0;

    scene_apply_gfx_state(this);
    
    for (i = 0; i < this->node_count; i++) {
        node_t *node = &this->nodes[i];
        
        if (node->collision_body.type == COLLISION_BODY_TYPE_BOX) {
            collision_box_t *collision_box = (collision_box_t *) node->collision_body.geometry;
            
            collision_box->position = node->translate;
        }
    }
}

void scene_routine(scene_t *this)
{
    unsigned int i;
    input_state_t *input_state;
    node_t *player_node;
    node_t *camera_node;

    input_state = &this->input_state;

    if (input_state->spec_keys & SPEC_KEY_ESCAPE) {
        this->is_active = false;
        return;
    }
    if ((input_state->spec_keys & SPEC_KEY_ALT) && (input_state->spec_keys & SPEC_KEY_ENTER)) {
        if (!toggling_fs) {
            toggling_fs = true;
            system_gfx_togglefs();
        }
    } else {
        toggling_fs = false;
    }

    player_node = &this->nodes[this->player_node];
    camera_node = &this->nodes[this->camera_node];

    player_node->routine(player_node, &this->nodes, &this->collision_queue, input_state, &this->node_count, &this->collision_count, this->camera_node, this->player_node);

    for (i = 0; i < this->node_count; i++) {
        node_t *node = &this->nodes[i];

        if (node != player_node && node->is_active && node->routine != NULL)
            node->routine(node, &this->nodes, &this->collision_queue, input_state, &this->node_count, &this->collision_count, this->camera_node, this->player_node);
    }

    free(this->collision_queue);
    this->collision_queue = NULL;
    this->collision_count = 0;
}

void scene_draw(const scene_t *this)
{
    unsigned int i;
    node_t *camera_node;
    coord_t camera_components;

    if (this->is_active) {
        scene_project(this);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        camera_node = &this->nodes[this->camera_node];
        vector_components(1.0, camera_node->rotate.axis.x, &camera_components);

        glRotated((GLdouble) camera_node->rotate.axis.x, 0.0, 1.0, 0.0);
        glRotated((GLdouble) camera_node->rotate.axis.y, (GLdouble) camera_components.x, 0.0, (GLdouble) camera_components.y);
        glTranslated((GLdouble) -camera_node->translate.x, (GLdouble) -camera_node->translate.y, (GLdouble) camera_node->translate.z);

        for (i = 0; i < this->node_count; i++) {
            node_t *node = &this->nodes[i];

            if (node->is_display) {
                glPushMatrix();
                    glTranslated((GLdouble) node->translate.x, (GLdouble) node->translate.y, (GLdouble) -node->translate.z);
                    glRotated((GLdouble) node->rotate.angle, (GLdouble) node->rotate.axis.x, (GLdouble) node->rotate.axis.y, (GLdouble) node->rotate.axis.z);
                    glScaled((GLdouble) node->scale.x, (GLdouble) node->scale.y, (GLdouble) node->scale.z);

                    if (!node->display_list)
                        node_compile(node);

                    glCallList(node->display_list);
                glPopMatrix();
            }
        }

        glFlush();
    }
}

void scene_free(scene_t *this)
{
    if (this->collision_count > 0) {
        free(this->collision_queue);
        this->collision_queue = NULL;
    }
    
    this->is_active = false;
}

/* applies the current gfx_state */
static void scene_apply_gfx_state(scene_t *this)
{
    unsigned int i;

    glClearColor((GLclampf) this->clear_color.red, (GLclampf) this->clear_color.green, (GLclampf) this->clear_color.blue, (GLclampf) this->clear_color.alpha);

    if (this->gfx_state & GFX_STATE_Z_BUFFER) {
        glEnable(GL_DEPTH_TEST);
    } else {
        glDisable(GL_DEPTH_TEST);
    }

    if (this->gfx_state & GFX_STATE_BACK_CULL) {
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
    } else {
        glDisable(GL_CULL_FACE);
    }

    if (this->gfx_state & GFX_STATE_FOG) {
        const float clear_color[4] = {
            (float) this->clear_color.red,
            (float) this->clear_color.green,
            (float) this->clear_color.blue,
            1.0f
        };

        glEnable(GL_FOG);
        glFogi(GL_FOG_MODE, this->fog_props->mode);
        glFogf(GL_FOG_DENSITY, this->fog_props->density);
        glFogf(GL_FOG_START, this->fog_props->start);
        glFogf(GL_FOG_END, this->fog_props->end);
        glFogfv(GL_FOG_COLOR, (GLfloat *) clear_color);
    } else {
        glDisable(GL_FOG);
    }

    if (this->gfx_state & GFX_STATE_LIGHTING) {
        glEnable(GL_LIGHTING);

        if (this->lighting_props->color_material)
            glEnable(GL_COLOR_MATERIAL);
        else
            glDisable(GL_COLOR_MATERIAL);

        for (i = 0; i < this->lighting_props->lights_count; i++) {
            const light_props_t *light_props = &this->lighting_props->lights_props[i];
            const GLfloat light_position[4] = {
                (GLfloat) light_props->position.x, (GLfloat) light_props->position.y, (GLfloat) light_props->position.z, 0.0f
            };
            const GLfloat light_direction[3] = {
                (GLfloat) light_props->direction.x, (GLfloat) light_props->direction.y, (GLfloat) light_props->direction.z
            };
            GLenum light = 0;
            bool have_light = true;

            switch (light_props->light) {
                case 0:
                    light = GL_LIGHT0;
                    break;
                default:
                    have_light = false;
                    break;
            }
            
            if (have_light) {
                glEnable(light);

                glLightfv(light, GL_POSITION, light_position);
                glLightfv(light, GL_SPOT_DIRECTION, light_direction);
            }
        }
    } else {
        glDisable(GL_LIGHTING);
    }
}

static void scene_project(const scene_t *this)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    if (this->is_ortho)
        glOrtho((GLdouble) this->projection.left, (GLdouble) this->projection.right, (GLdouble) this->projection.bottom, (GLdouble) this->projection.top, (GLdouble) this->projection.near_plane, (GLdouble) this->projection.far_plane);
    else
        glFrustum((GLdouble) this->projection.left, (GLdouble) this->projection.right, (GLdouble) this->projection.bottom, (GLdouble) this->projection.top, (GLdouble) this->projection.near_plane, (GLdouble) this->projection.far_plane);
}
