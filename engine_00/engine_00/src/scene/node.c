#if defined(SYSTEM_GLX)
#include <GL/gl.h>
#elif defined(SYSTEM_CGL)
#include <OpenGL/gl.h>
#elif defined(SYSTEM_WGL)
#include <Windows.h>
#include <gl/GL.h>
#else
#error "The current system is not supported"
#endif /* SYSTEM_GLX | SYSTEM_CGL | SYSTEM_WGL */

#include "mesh.h"

#include "node.h"

void node_compile(node_t *this)
{
    this->display_list = glGenLists(1);

    glNewList(this->display_list, GL_COMPILE);
        mesh_draw(this->mesh);
    glEndList();
}
