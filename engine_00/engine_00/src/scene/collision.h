#ifndef COLLISION_H
#define COLLISION_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stdbool.h>

#include "../system/primitives.h"

typedef enum collision_body_type {
    COLLISION_BODY_TYPE_NONE,
    COLLISION_BODY_TYPE_BOX
} collision_body_type_t;

typedef struct collision_body {
    collision_body_type_t type;
    bool reactive;
    void *geometry;
    unsigned int node_id;
} collision_body_t;

typedef struct collision_props {
    bool is_landing;
    bool is_ceiling;
} collision_props_t;

typedef struct collision {
    const collision_body_t *src;
    const collision_body_t *dest;
    bool processed;
} collision_t;

typedef struct collision_box {
    coord_t position;
    coord_t size;
    double radius;
} collision_box_t;

/* determine if two bodies have collided */
extern bool collision_determine(collision_body_t *body_a, collision_body_t *body_b);

/* registers a given collision in a provided queue with size */
extern void collision_register(collision_body_t *src, collision_body_t *dest, collision_t **collision_queue, unsigned int *collision_count);

#ifdef __cplusplus
};
#endif /* __cplusplus */

#endif /* COLLISION_H */
