#ifndef PHYSICS_H
#define PHYSICS_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "../system/primitives.h"

/* general physics properties */
typedef struct physics_props {
    coord_t velocity;
    coord_t acceleration;
    double friction_factor;
    double terminal_velocity;
    double gravity_factor;
    coord_t last_position;
} physics_props_t;

/* applies various physics calcs */
extern coord_t physics_apply_motion(physics_props_t *physics_props, bool on_ground);

#ifdef __cplusplus
};
#endif /* __cplusplus */

#endif /* PHYSICS_H */
