#ifndef SCENE_H
#define SCENE_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stdbool.h>

#if defined(SYSTEM_GLX)
#include <GL/gl.h>
#elif defined(SYSTEM_CGL)
#include <OpenGL/gl.h>
#elif defined(SYSTEM_WGL)
#include <Windows.h>
#include <gl/GL.h>
#else
#error "The current system is not supported"
#endif /* SYSTEM_GLX | SYSTEM_CGL | SYSTEM_WGL */

#include "../system/system.h"
#include "../system/primitives.h"

#include "node.h"
#include "collision.h"

/* graphics subsystem features */
typedef unsigned char gfx_state_t;
#define GFX_STATE_Z_BUFFER  0x01
#define GFX_STATE_BACK_CULL 0x02
#define GFX_STATE_FOG       0x04
#define GFX_STATE_LIGHTING  0x08

/* fog properties */
typedef struct fog_props {
    GLint mode;
    float density;
    float start;
    float end;
} fog_props_t;

/* individual light properties */
typedef struct light_props {
    int light;
    coord_t position;
    coord_t direction;
} light_props_t;

/* lighting system properties */
typedef struct lighting_props {
    bool color_material;
    light_props_t *lights_props;
    unsigned int lights_count;
} lighting_props_t;

/* a single scene with node collection */
typedef struct scene {
    node_t *nodes;
    unsigned int node_count;
    unsigned int player_node;
    unsigned int camera_node;
    rgba_t clear_color;
    projection_t projection;
    bool is_ortho;
    gfx_state_t gfx_state;
    input_state_t input_state;
    bool is_active;
    fog_props_t *fog_props;
    lighting_props_t *lighting_props;
    collision_t *collision_queue;
    unsigned int collision_count;
} scene_t;

/* initialize a scene based on state */
extern void scene_init(scene_t *this);

/* process the actions of a frame of a scene */
extern void scene_routine(scene_t *this);

/* draw the results of a frame of routine */
extern void scene_draw(const scene_t *this);

/* free the resources of a scene */
extern void scene_free(scene_t *this);

#ifdef __cplusplus
};
#endif /* __cplusplus */

#endif /* SCENE_H */
