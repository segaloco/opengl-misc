#ifndef MESH_H
#define MESH_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#if defined(SYSTEM_GLX)
#include <GL/gl.h>
#elif defined(SYSTEM_CGL)
#include <OpenGL/gl.h>
#elif defined(SYSTEM_WGL)
#include <Windows.h>
#include <gl/GL.h>
#else
#error "The current system is not supported"
#endif /* SYSTEM_GLX | SYSTEM_CGL | SYSTEM_WGL */

#include "../system/primitives.h"

/* a single mesh of vertices with metadata */
typedef struct mesh {
    coord_t *vertices;
    coord_t *normals;
    rgba_t *colors;
    unsigned int vertex_count;
    GLenum mode;
} mesh_t;

/* draw a standard mesh */
extern void mesh_draw(mesh_t *this);

#ifdef __cplusplus
};
#endif /* __cplusplus */

#endif /* MESH_H */
