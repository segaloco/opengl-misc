#ifndef NODE_H
#define NODE_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stdbool.h>

#if defined(SYSTEM_GLX)
#include <GL/gl.h>
#elif defined(SYSTEM_CGL)
#include <OpenGL/gl.h>
#elif defined(SYSTEM_WGL)
#include <Windows.h>
#include <gl/GL.h>
#else
#error "The current system is not supported"
#endif /* SYSTEM_GLX | SYSTEM_CGL | SYSTEM_WGL */

#include "../system/system.h"
#include "../system/primitives.h"

#include "mesh.h"
#include "collision.h"
#include "physics.h"

/* a node in the scene graph */
typedef struct node {
    unsigned int id;
    mesh_t *mesh;
    coord_t scale;
    rotate_t rotate;
    coord_t translate;
    GLuint display_list;
    bool is_active;
    bool is_display;
    physics_props_t physics_props;
    void *properties_ext;
    collision_body_t collision_body;
    void (*routine)(struct node *this, struct node **tree, collision_t **col_queue, input_state_t *input_state, unsigned int *tree_size, unsigned int *col_queue_size, unsigned int camera_node_index, unsigned int player_node_index);
} node_t;

/* compile a node's display list */
extern void node_compile(node_t *this);

#ifdef __cplusplus
};
#endif /* __cplusplus */

#endif /* NODE_H */
