#include "system/system.h"

#include "scene/scene.h"

#include "scenes/scenes.h"

int main(int argc, char *argv[])
{
    unsigned int scene_index = 0;
    scene_t *current_scene = scenes[scene_index];

    system_init(640, 480, "Game");

    scene_init(current_scene);

    while (current_scene->is_active) {
        unsigned int next_scene_index = 0;
        
        system_input_poll(&current_scene->input_state);

        scene_routine(current_scene);
        scene_draw(current_scene);

        system_gfx_swap();
        
        if (scene_index != next_scene_index) {
            scene_index = next_scene_index;
            
            scene_free(current_scene);
            
            current_scene = scenes[scene_index];
            scene_init(current_scene);
        }
    }

    system_free();

    return 0;
}
