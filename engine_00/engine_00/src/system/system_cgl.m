#import <Foundation/Foundation.h>
#import <Carbon/Carbon.h>
#import <Cocoa/Cocoa.h>
#import <OpenGL/OpenGL.h>

#include "system.h"

static unsigned int w_xres, w_yres;

static NSApplication *application;
static NSBundle *bundle;
static NSWindow *window;
static NSOpenGLView *view;

static BOOL isFullScreen = NO;

void system_init(unsigned int xres, unsigned int yres, const char *title)
{
    NSRect frame;
    NSOpenGLPixelFormat *pixelFormat;
    NSWindowStyleMask styleMask;
    NSString *titleString;

    const NSOpenGLPixelFormatAttribute attributes[] = {
        NSOpenGLPFADoubleBuffer,
        NSOpenGLPFAColorSize, 24,
        NSOpenGLPFAAlphaSize, 8,
        NSOpenGLPFADepthSize, 24,
        NSOpenGLPFAStencilSize, 8,
        0
    };

    w_xres = xres;
    w_yres = yres;
    
    application = [NSApplication sharedApplication];
    [application setActivationPolicy:NSApplicationActivationPolicyRegular];

    frame = NSMakeRect(100, 100, xres, yres);
    pixelFormat = [[NSOpenGLPixelFormat alloc] initWithAttributes:attributes];
    titleString = [[NSString alloc] initWithCString:title encoding:NSUTF8StringEncoding];
    
    styleMask = NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable;
    window = [[NSWindow alloc] initWithContentRect:frame
            styleMask:styleMask
            backing:NSBackingStoreBuffered
            defer:NO];
    view = [[NSOpenGLView alloc]
        initWithFrame:frame
        pixelFormat:pixelFormat];
    [window setContentView:view];
    [[view openGLContext] makeCurrentContext];
    
    [window setTitle:titleString];
    [window makeKeyAndOrderFront:application];
    [application activateIgnoringOtherApps:YES];
    [application finishLaunching];

    return;
}

void system_free(void)
{
    [application terminate:window];
    
    return;
}

void system_gfx_swap(void)
{
    [[view openGLContext] flushBuffer];

    return;
}

void system_gfx_togglefs(void)
{
    BOOL toggleFullScreen;
    
    toggleFullScreen = isFullScreen ? NO : YES;
    
    [window toggleFullScreen:window];
    
    isFullScreen = !isFullScreen;
    
    return;
}

void system_input_poll(input_state_t *state)
{
    NSEvent *event = [application
        nextEventMatchingMask:NSEventMaskAny
        untilDate:nil
        inMode:NSDefaultRunLoopMode
        dequeue:YES];

    switch ([event type]) {
    case NSEventTypeKeyDown:
        switch ([event keyCode]) {
        case kVK_ANSI_A:
            state->letter_keys |= LETTER_KEY_A;
            break;
        case kVK_ANSI_B:
            state->letter_keys |= LETTER_KEY_B;
            break;
        case kVK_ANSI_C:
            state->letter_keys |= LETTER_KEY_C;
            break;
        case kVK_ANSI_D:
            state->letter_keys |= LETTER_KEY_D;
            break;
        case kVK_ANSI_E:
            state->letter_keys |= LETTER_KEY_E;
            break;
        case kVK_ANSI_F:
            state->letter_keys |= LETTER_KEY_F;
            break;
        case kVK_ANSI_G:
            state->letter_keys |= LETTER_KEY_G;
            break;
        case kVK_ANSI_H:
            state->letter_keys |= LETTER_KEY_H;
            break;
        case kVK_ANSI_I:
            state->letter_keys |= LETTER_KEY_I;
            break;
        case kVK_ANSI_J:
            state->letter_keys |= LETTER_KEY_J;
            break;
        case kVK_ANSI_K:
            state->letter_keys |= LETTER_KEY_K;
            break;
        case kVK_ANSI_L:
            state->letter_keys |= LETTER_KEY_L;
            break;
        case kVK_ANSI_M:
            state->letter_keys |= LETTER_KEY_M;
            break;
        case kVK_ANSI_N:
            state->letter_keys |= LETTER_KEY_N;
            break;
        case kVK_ANSI_O:
            state->letter_keys |= LETTER_KEY_O;
            break;
        case kVK_ANSI_P:
            state->letter_keys |= LETTER_KEY_P;
            break;
        case kVK_ANSI_Q:
            state->letter_keys |= LETTER_KEY_Q;
            break;
        case kVK_ANSI_R:
            state->letter_keys |= LETTER_KEY_R;
            break;
        case kVK_ANSI_S:
            state->letter_keys |= LETTER_KEY_S;
            break;
        case kVK_ANSI_T:
            state->letter_keys |= LETTER_KEY_T;
            break;
        case kVK_ANSI_U:
            state->letter_keys |= LETTER_KEY_U;
            break;
        case kVK_ANSI_V:
            state->letter_keys |= LETTER_KEY_V;
            break;
        case kVK_ANSI_W:
            state->letter_keys |= LETTER_KEY_W;
            break;
        case kVK_ANSI_X:
            state->letter_keys |= LETTER_KEY_X;
            break;
        case kVK_ANSI_Y:
            state->letter_keys |= LETTER_KEY_Y;
            break;
        case kVK_ANSI_Z:
            state->letter_keys |= LETTER_KEY_Z;
            break;
        case kVK_ANSI_0:
            state->num_keys |= NUM_KEY_0;
            break;
        case kVK_ANSI_1:
            state->num_keys |= NUM_KEY_1;
            break;
        case kVK_ANSI_2:
            state->num_keys |= NUM_KEY_2;
            break;
        case kVK_ANSI_3:
            state->num_keys |= NUM_KEY_3;
            break;
        case kVK_ANSI_4:
            state->num_keys |= NUM_KEY_4;
            break;
        case kVK_ANSI_5:
            state->num_keys |= NUM_KEY_5;
            break;
        case kVK_ANSI_6:
            state->num_keys |= NUM_KEY_6;
            break;
        case kVK_ANSI_7:
            state->num_keys |= NUM_KEY_7;
            break;
        case kVK_ANSI_8:
            state->num_keys |= NUM_KEY_8;
            break;
        case kVK_ANSI_9:
            state->num_keys |= NUM_KEY_9;
            break;
        case kVK_Return:
            state->spec_keys |= SPEC_KEY_ENTER;
            break;
        case kVK_Escape:
            state->spec_keys |= SPEC_KEY_ESCAPE;
            break;
        case kVK_Space:
            state->spec_keys |= SPEC_KEY_SPACE;
            break;
        case kVK_Delete:
            state->spec_keys |= SPEC_KEY_BKSP;
            break;
        case kVK_Tab:
            state->spec_keys |= SPEC_KEY_TAB;
            break;
        default:
            break;
        }
        break;
    case NSEventTypeKeyUp:
        switch ([event keyCode]) {
        case kVK_ANSI_A:
            state->letter_keys &= ~LETTER_KEY_A;
            break;
        case kVK_ANSI_B:
            state->letter_keys &= ~LETTER_KEY_B;
            break;
        case kVK_ANSI_C:
            state->letter_keys &= ~LETTER_KEY_C;
            break;
        case kVK_ANSI_D:
            state->letter_keys &= ~LETTER_KEY_D;
            break;
        case kVK_ANSI_E:
            state->letter_keys &= ~LETTER_KEY_E;
            break;
        case kVK_ANSI_F:
            state->letter_keys &= ~LETTER_KEY_F;
            break;
        case kVK_ANSI_G:
            state->letter_keys &= ~LETTER_KEY_G;
            break;
        case kVK_ANSI_H:
            state->letter_keys &= ~LETTER_KEY_H;
            break;
        case kVK_ANSI_I:
            state->letter_keys &= ~LETTER_KEY_I;
            break;
        case kVK_ANSI_J:
            state->letter_keys &= ~LETTER_KEY_J;
            break;
        case kVK_ANSI_K:
            state->letter_keys &= ~LETTER_KEY_K;
            break;
        case kVK_ANSI_L:
            state->letter_keys &= ~LETTER_KEY_L;
            break;
        case kVK_ANSI_M:
            state->letter_keys &= ~LETTER_KEY_M;
            break;
        case kVK_ANSI_N:
            state->letter_keys &= ~LETTER_KEY_N;
            break;
        case kVK_ANSI_O:
            state->letter_keys &= ~LETTER_KEY_O;
            break;
        case kVK_ANSI_P:
            state->letter_keys &= ~LETTER_KEY_P;
            break;
        case kVK_ANSI_Q:
            state->letter_keys &= ~LETTER_KEY_Q;
            break;
        case kVK_ANSI_R:
            state->letter_keys &= ~LETTER_KEY_R;
            break;
        case kVK_ANSI_S:
            state->letter_keys &= ~LETTER_KEY_S;
            break;
        case kVK_ANSI_T:
            state->letter_keys &= ~LETTER_KEY_T;
            break;
        case kVK_ANSI_U:
            state->letter_keys &= ~LETTER_KEY_U;
            break;
        case kVK_ANSI_V:
            state->letter_keys &= ~LETTER_KEY_V;
            break;
        case kVK_ANSI_W:
            state->letter_keys &= ~LETTER_KEY_W;
            break;
        case kVK_ANSI_X:
            state->letter_keys &= ~LETTER_KEY_X;
            break;
        case kVK_ANSI_Y:
            state->letter_keys &= ~LETTER_KEY_Y;
            break;
        case kVK_ANSI_Z:
            state->letter_keys &= ~LETTER_KEY_Z;
            break;
        case kVK_ANSI_0:
            state->num_keys &= ~NUM_KEY_0;
            break;
        case kVK_ANSI_1:
            state->num_keys &= ~NUM_KEY_1;
            break;
        case kVK_ANSI_2:
            state->num_keys &= ~NUM_KEY_2;
            break;
        case kVK_ANSI_3:
            state->num_keys &= ~NUM_KEY_3;
            break;
        case kVK_ANSI_4:
            state->num_keys &= ~NUM_KEY_4;
            break;
        case kVK_ANSI_5:
            state->num_keys &= ~NUM_KEY_5;
            break;
        case kVK_ANSI_6:
            state->num_keys &= ~NUM_KEY_6;
            break;
        case kVK_ANSI_7:
            state->num_keys &= ~NUM_KEY_7;
            break;
        case kVK_ANSI_8:
            state->num_keys &= ~NUM_KEY_8;
            break;
        case kVK_ANSI_9:
            state->num_keys &= ~NUM_KEY_9;
            break;
        case kVK_Return:
            state->spec_keys &= ~SPEC_KEY_ENTER;
            break;
        case kVK_Escape:
            state->spec_keys &= ~SPEC_KEY_ESCAPE;
            break;
        case kVK_Space:
            state->spec_keys &= ~SPEC_KEY_SPACE;
            break;
        case kVK_Delete:
            state->spec_keys &= ~SPEC_KEY_BKSP;
            break;
        case kVK_Tab:
            state->spec_keys &= ~SPEC_KEY_TAB;
            break;
        default:
            break;
        }
        break;
    case NSEventTypeFlagsChanged:
        if ([event modifierFlags] & NSEventModifierFlagCommand)
            state->spec_keys |= SPEC_KEY_ALT;
        else
            state->spec_keys &= ~SPEC_KEY_ALT;
            
        if ([event modifierFlags] & NSEventModifierFlagControl)
            state->spec_keys |= SPEC_KEY_CTRL;
        else
            state->spec_keys &= ~SPEC_KEY_CTRL;
        break;
    case NSEventTypeLeftMouseDown: {
            NSPoint mouseLocation;
        
            NSButton *closeButton;
            NSRect closeButtonCoords;
            CGFloat leftBound, rightBound, upperBound, lowerBound;

            mouseLocation = [window mouseLocationOutsideOfEventStream];

            closeButton = [window standardWindowButton:NSWindowCloseButton];
            closeButtonCoords = [closeButton convertRect:[closeButton bounds] toView:nil];
            
            leftBound = closeButtonCoords.origin.x;
            rightBound = closeButtonCoords.origin.x + closeButtonCoords.size.width;
            lowerBound = closeButtonCoords.origin.y;
            upperBound = closeButtonCoords.origin.y + closeButtonCoords.size.height;
                
            if (mouseLocation.x > leftBound && mouseLocation.x < rightBound && mouseLocation.y > lowerBound && mouseLocation.y < upperBound)
                state->spec_keys |= SPEC_KEY_ESCAPE;
        }
        break;
    default:
        break;
    }

    [application sendEvent:event];

    return;
}
