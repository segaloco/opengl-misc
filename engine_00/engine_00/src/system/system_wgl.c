#include <stdbool.h>

#include <Windows.h>
#include <gl/GL.h>
#include <gl/wglext.h>

#include "system.h"

static PFNWGLSWAPINTERVALEXTPROC wglSwapInterval;
static const LPCTSTR wndClassName = L"WndClass";
static HINSTANCE hInstance;
static WNDCLASS wndClass = { 0 };
static HWND hWndMain;
static HDC hDc;
static HGLRC hGLrc;
static bool isClosing = false;

static LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg) {
    case WM_CLOSE:
        isClosing = true;
        break;
	default:
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

void system_init(unsigned int xres, unsigned int yres, const char *title)
{
    PIXELFORMATDESCRIPTOR pfd = {
        sizeof(PIXELFORMATDESCRIPTOR)
        ,1
        ,PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER | PFD_SWAP_LAYER_BUFFERS
        ,PFD_TYPE_RGBA
        ,32
        ,0, 0, 0, 0, 0, 0
        ,0
        ,0
        ,0
        ,0, 0, 0, 0
        ,24
        ,8
        ,0
        ,PFD_MAIN_PLANE
        ,0
        ,0, 0, 0
    };
    int pixelFormat;

	hInstance = GetModuleHandle(NULL);

	wndClass.style = CS_OWNDC | CS_VREDRAW | CS_HREDRAW;
	wndClass.lpfnWndProc = WndProc;
	wndClass.hInstance = hInstance;
	wndClass.hbrBackground = (HBRUSH) COLOR_BACKGROUND;
	wndClass.lpszClassName = wndClassName;

	RegisterClass(&wndClass);

	hWndMain = CreateWindow(
		wndClassName,
		L"Test",
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		xres,
		yres,
		NULL,
		NULL,
		hInstance,
		NULL
	);

    hDc = GetDC(hWndMain);

    pixelFormat = ChoosePixelFormat(hDc, &pfd);
    SetPixelFormat(hDc, pixelFormat, &pfd);

    hGLrc = wglCreateContext(hDc);
    wglMakeCurrent(hDc, hGLrc);

    //wglSwapInterval = (PFNWGLSWAPINTERVALEXTPROC)wglGetProcAddress("wglSwapIntervalEXT");
    //wglSwapInterval(1);

	ShowWindow(hWndMain, SW_SHOW);
}

void system_free(void)
{
	ShowWindow(hWndMain, SW_HIDE);

	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(hGLrc);

	ReleaseDC(hWndMain, hDc);

	DestroyWindow(hWndMain);

	UnregisterClass(wndClassName, hInstance);

	PostQuitMessage(0);
}

void system_gfx_swap(void)
{
	wglSwapLayerBuffers(hDc, WGL_SWAP_MAIN_PLANE);
}

void system_gfx_togglefs(void)
{ }

void system_input_poll(input_state_t *state)
{
	MSG msg = { 0 };

	while (PeekMessage(&msg, hWndMain, 0, 0, PM_REMOVE)) {
		TranslateMessage(&msg);

		switch (msg.message) {
		case WM_KEYDOWN:
			switch (msg.wParam) {
            case 'A':
            case 'a':
                state->letter_keys |= LETTER_KEY_A;
                break;
            case 'B':
            case 'b':
                state->letter_keys |= LETTER_KEY_B;
                break;
            case 'C':
            case 'c':
                state->letter_keys |= LETTER_KEY_C;
                break;
            case 'D':
            case 'd':
                state->letter_keys |= LETTER_KEY_D;
                break;
            case 'E':
            case 'e':
                state->letter_keys |= LETTER_KEY_E;
                break;
            case 'F':
            case 'f':
                state->letter_keys |= LETTER_KEY_F;
                break;
            case 'G':
            case 'g':
                state->letter_keys |= LETTER_KEY_G;
                break;
            case 'H':
            case 'h':
                state->letter_keys |= LETTER_KEY_H;
                break;
            case 'I':
            case 'i':
                state->letter_keys |= LETTER_KEY_I;
                break;
            case 'J':
            case 'j':
                state->letter_keys |= LETTER_KEY_J;
                break;
            case 'K':
            case 'k':
                state->letter_keys |= LETTER_KEY_K;
                break;
            case 'L':
            case 'l':
                state->letter_keys |= LETTER_KEY_L;
                break;
            case 'M':
            case 'm':
                state->letter_keys |= LETTER_KEY_M;
                break;
            case 'N':
            case 'n':
                state->letter_keys |= LETTER_KEY_N;
                break;
            case 'O':
            case 'o':
                state->letter_keys |= LETTER_KEY_O;
                break;
            case 'P':
            case 'p':
                state->letter_keys |= LETTER_KEY_P;
                break;
            case 'Q':
            case 'q':
                state->letter_keys |= LETTER_KEY_Q;
                break;
            case 'R':
            case 'r':
                state->letter_keys |= LETTER_KEY_R;
                break;
            case 'S':
            case 's':
                state->letter_keys |= LETTER_KEY_S;
                break;
            case 'T':
            case 't':
                state->letter_keys |= LETTER_KEY_T;
                break;
            case 'U':
            case 'u':
                state->letter_keys |= LETTER_KEY_U;
                break;
            case 'V':
            case 'v':
                state->letter_keys |= LETTER_KEY_V;
                break;
            case 'W':
            case 'w':
                state->letter_keys |= LETTER_KEY_W;
                break;
            case 'X':
            case 'x':
                state->letter_keys |= LETTER_KEY_X;
                break;
            case 'Y':
            case 'y':
                state->letter_keys |= LETTER_KEY_Y;
                break;
            case 'Z':
            case 'z':
                state->letter_keys |= LETTER_KEY_Z;
                break;
            case '0':
                state->num_keys |= NUM_KEY_0;
                break;
            case '1':
                state->num_keys |= NUM_KEY_1;
                break;
            case '2':
                state->num_keys |= NUM_KEY_2;
                break;
            case '3':
                state->num_keys |= NUM_KEY_3;
                break;
            case '4':
                state->num_keys |= NUM_KEY_4;
                break;
            case '5':
                state->num_keys |= NUM_KEY_5;
                break;
            case '6':
                state->num_keys |= NUM_KEY_6;
                break;
            case '7':
                state->num_keys |= NUM_KEY_7;
                break;
            case '8':
                state->num_keys |= NUM_KEY_8;
                break;
            case '9':
                state->num_keys |= NUM_KEY_9;
                break;
            case VK_MENU:
                state->spec_keys |= SPEC_KEY_ALT;
                break;
            case VK_CONTROL:
                state->spec_keys |= SPEC_KEY_CTRL;
                break;
            case VK_RETURN:
                state->spec_keys |= SPEC_KEY_ENTER;
                break;
            case VK_ESCAPE:
                state->spec_keys |= SPEC_KEY_ESCAPE;
                break;
            case VK_SPACE:
                state->spec_keys |= SPEC_KEY_SPACE;
                break;
            case VK_BACK:
                state->spec_keys |= SPEC_KEY_BKSP;
                break;
            case VK_TAB:
                state->spec_keys |= SPEC_KEY_TAB;
                break;
            case VK_DELETE:
                state->spec_keys |= SPEC_KEY_DEL;
                break;
			default:
				break;
			}
			break;
		case WM_KEYUP:
			switch (msg.wParam) {
            case 'A':
            case 'a':
                state->letter_keys &= ~LETTER_KEY_A;
                break;
            case 'B':
            case 'b':
                state->letter_keys &= ~LETTER_KEY_B;
                break;
            case 'C':
            case 'c':
                state->letter_keys &= ~LETTER_KEY_C;
                break;
            case 'D':
            case 'd':
                state->letter_keys &= ~LETTER_KEY_D;
                break;
            case 'E':
            case 'e':
                state->letter_keys &= ~LETTER_KEY_E;
                break;
            case 'F':
            case 'f':
                state->letter_keys &= ~LETTER_KEY_F;
                break;
            case 'G':
            case 'g':
                state->letter_keys &= ~LETTER_KEY_G;
                break;
            case 'H':
            case 'h':
                state->letter_keys &= ~LETTER_KEY_H;
                break;
            case 'I':
            case 'i':
                state->letter_keys &= ~LETTER_KEY_I;
                break;
            case 'J':
            case 'j':
                state->letter_keys &= ~LETTER_KEY_J;
                break;
            case 'K':
            case 'k':
                state->letter_keys &= ~LETTER_KEY_K;
                break;
            case 'L':
            case 'l':
                state->letter_keys &= ~LETTER_KEY_L;
                break;
            case 'M':
            case 'm':
                state->letter_keys &= ~LETTER_KEY_M;
                break;
            case 'N':
            case 'n':
                state->letter_keys &= ~LETTER_KEY_N;
                break;
            case 'O':
            case 'o':
                state->letter_keys &= ~LETTER_KEY_O;
                break;
            case 'P':
            case 'p':
                state->letter_keys &= ~LETTER_KEY_P;
                break;
            case 'Q':
            case 'q':
                state->letter_keys &= ~LETTER_KEY_Q;
                break;
            case 'R':
            case 'r':
                state->letter_keys &= ~LETTER_KEY_R;
                break;
            case 'S':
            case 's':
                state->letter_keys &= ~LETTER_KEY_S;
                break;
            case 'T':
            case 't':
                state->letter_keys &= ~LETTER_KEY_T;
                break;
            case 'U':
            case 'u':
                state->letter_keys &= ~LETTER_KEY_U;
                break;
            case 'V':
            case 'v':
                state->letter_keys &= ~LETTER_KEY_V;
                break;
            case 'W':
            case 'w':
                state->letter_keys &= ~LETTER_KEY_W;
                break;
            case 'X':
            case 'x':
                state->letter_keys &= ~LETTER_KEY_X;
                break;
            case 'Y':
            case 'y':
                state->letter_keys &= ~LETTER_KEY_Y;
                break;
            case 'Z':
            case 'z':
                state->letter_keys &= ~LETTER_KEY_Z;
                break;
            case '0':
                state->num_keys &= ~NUM_KEY_0;
                break;
            case '1':
                state->num_keys &= ~NUM_KEY_1;
                break;
            case '2':
                state->num_keys &= ~NUM_KEY_2;
                break;
            case '3':
                state->num_keys &= ~NUM_KEY_3;
                break;
            case '4':
                state->num_keys &= ~NUM_KEY_4;
                break;
            case '5':
                state->num_keys &= ~NUM_KEY_5;
                break;
            case '6':
                state->num_keys &= ~NUM_KEY_6;
                break;
            case '7':
                state->num_keys &= ~NUM_KEY_7;
                break;
            case '8':
                state->num_keys &= ~NUM_KEY_8;
                break;
            case '9':
                state->num_keys &= ~NUM_KEY_9;
                break;
            case VK_MENU:
                state->spec_keys &= ~SPEC_KEY_ALT;
                break;
            case VK_CONTROL:
                state->spec_keys &= ~SPEC_KEY_CTRL;
                break;
            case VK_RETURN:
                state->spec_keys &= ~SPEC_KEY_ENTER;
                break;
            case VK_ESCAPE:
                state->spec_keys &= ~SPEC_KEY_ESCAPE;
                break;
            case VK_SPACE:
                state->spec_keys &= ~SPEC_KEY_SPACE;
                break;
            case VK_BACK:
                state->spec_keys &= ~SPEC_KEY_BKSP;
                break;
            case VK_TAB:
                state->spec_keys &= ~SPEC_KEY_TAB;
                break;
            case VK_DELETE:
                state->spec_keys &= ~SPEC_KEY_DEL;
                break;
			default:
				break;
			}
		default:
			break;
		}

        DispatchMessage(&msg);

        if (isClosing)
            state->spec_keys |= SPEC_KEY_ESCAPE;
	}
}
