#include <math.h>

#include "primitives.h"

#define PI 3.141592653589793

#define degtorad(deg) (deg / 360.0) * 2.0 * PI
#define radtodeg(rad) (rad * 180.0) / PI

void vector_components(double magnitude, double angle, coord_t *components)
{
    components->x = magnitude * cos(degtorad(angle));
    components->y = magnitude * sin(degtorad(angle));
}

double vector_rotate(double x, double y)
{
    double angle;

    if (y == 0.0) {
        if (x > 0.0)
            return 90.0;
        else if (x < 0.0)
            return -90.0;
        else
            return 0.0;
    }

    if (x == 0.0 && y < 0.0)
        return 180.0;

    angle = radtodeg(atan(x / y));

    if (y < 0.0) {
        if (x > 0.0)
            return angle + 180.0;
        else
            return angle - 180.0;
    }

    return angle;
}
