#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include <X11/Xlib.h>
#include <X11/keysym.h>
#include <GL/gl.h>
#include <GL/glx.h>

#include "system.h"

static unsigned int w_xres, w_yres;

static Display *dpy;
static int default_screen;
static GLXFBConfig *glx_config;
static XSetWindowAttributes window_attributes = { 0 };
static Window win;
static GLXWindow glx_win;
static GLXContext context;

static bool is_fullscreen = false;

void system_init(unsigned int xres, unsigned int yres, const char *title)
{
    int i;
    GLXFBConfig *glx_configs;
    int glx_configs_count;
    XVisualInfo *visual_info;
    unsigned long window_attribute_mask;

    const int attrib_list[] = {
        GLX_DOUBLEBUFFER,   True,
        GLX_RED_SIZE,       8,
        GLX_GREEN_SIZE,     8,
        GLX_BLUE_SIZE,      8,
        GLX_ALPHA_SIZE,     8,
        GLX_DEPTH_SIZE,     24,
        GLX_STENCIL_SIZE,   8,
        GLX_RENDER_TYPE,    GLX_RGBA_BIT,
        GLX_DRAWABLE_TYPE,  GLX_WINDOW_BIT,
        GLX_X_RENDERABLE,   True,
        None
    };

    w_xres = xres;
    w_yres = yres;

    dpy = XOpenDisplay(NULL);
    default_screen = XDefaultScreen(dpy);

    glx_configs = glXChooseFBConfig(dpy, default_screen, attrib_list, &glx_configs_count);
    glx_config = malloc(sizeof (*glx_config));
    for (i = 0; i < glx_configs_count; i++) {
        if (i == 0) {
            memcpy(glx_config, &glx_configs[i], sizeof (*glx_config));
            break;
        }
    }
    XFree(glx_configs);

    visual_info = glXGetVisualFromFBConfig(dpy, *glx_config);
    window_attribute_mask = CWEventMask | CWOverrideRedirect;
    window_attributes.event_mask = KeyPressMask | KeyReleaseMask | StructureNotifyMask;
    window_attributes.override_redirect = False;
    win = XCreateWindow(
        dpy,
        XDefaultRootWindow(dpy),
        100,
        100,
        xres,
        yres,
        0,
        CopyFromParent,
        CopyFromParent,
        visual_info->visual,
        window_attribute_mask,
        &window_attributes
    );
    XFree(visual_info);

    glx_win = glXCreateWindow(dpy, *glx_config, win, NULL);
    context = glXCreateNewContext(dpy, *glx_config, GLX_RGBA_TYPE, NULL, True);
    glXMakeContextCurrent(dpy, glx_win, glx_win, context);

    XStoreName(dpy, win, title);
    XMapWindow(dpy, win);
    XGrabKeyboard(dpy, win, False, GrabModeAsync, GrabModeAsync, CurrentTime);

    return;
}

void system_free(void)
{
    window_attributes.override_redirect = False;

    XUngrabKeyboard(dpy, CurrentTime);
    XUnmapWindow(dpy, win);

    glXMakeContextCurrent(dpy, None, None, None);
    glXDestroyContext(dpy, context);
    glXDestroyWindow(dpy, glx_win);

    XChangeWindowAttributes(dpy, win, CWOverrideRedirect, &window_attributes);
    XDestroyWindow(dpy, win);

    free(glx_config);

    XCloseDisplay(dpy);

    return;
}

void system_gfx_swap(void)
{
    glXSwapBuffers(dpy, glx_win);

    return;
}

void system_gfx_togglefs(void)
{
    if (is_fullscreen)
        XUngrabKeyboard(dpy, CurrentTime);

    XUnmapWindow(dpy, win);

    glXMakeContextCurrent(dpy, None, None, None);
    glXDestroyWindow(dpy, glx_win);

    if (!is_fullscreen) {
        window_attributes.override_redirect = True;

        XChangeWindowAttributes(dpy, win, CWOverrideRedirect, &window_attributes);
		XMoveResizeWindow(dpy, win, 0, 0, XDisplayWidth(dpy, default_screen), XDisplayHeight(dpy, default_screen));
    } else {
        window_attributes.override_redirect = False;

        XChangeWindowAttributes(dpy, win, CWOverrideRedirect, &window_attributes);
        XResizeWindow(dpy, win, w_xres, w_yres);
    }

    glx_win = glXCreateWindow(dpy, *glx_config, win, NULL);
    glXMakeContextCurrent(dpy, glx_win, glx_win, context);

    XMapWindow(dpy, win);
    XGrabKeyboard(dpy, win, False, GrabModeAsync, GrabModeAsync, CurrentTime);

    is_fullscreen = !is_fullscreen;

    return;
}

void system_input_poll(input_state_t *state)
{
    XEvent event;

    while (XPending(dpy) > 0) {
        XNextEvent(dpy, &event);

        switch (event.type) {
        case KeyPress:
            switch (XLookupKeysym(&event.xkey, 0)) {
            case XK_A:
            case XK_a:
                state->letter_keys |= LETTER_KEY_A;
                break;
            case XK_B:
            case XK_b:
                state->letter_keys |= LETTER_KEY_B;
                break;
            case XK_C:
            case XK_c:
                state->letter_keys |= LETTER_KEY_C;
                break;
            case XK_D:
            case XK_d:
                state->letter_keys |= LETTER_KEY_D;
                break;
            case XK_E:
            case XK_e:
                state->letter_keys |= LETTER_KEY_E;
                break;
            case XK_F:
            case XK_f:
                state->letter_keys |= LETTER_KEY_F;
                break;
            case XK_G:
            case XK_g:
                state->letter_keys |= LETTER_KEY_G;
                break;
            case XK_H:
            case XK_h:
                state->letter_keys |= LETTER_KEY_H;
                break;
            case XK_I:
            case XK_i:
                state->letter_keys |= LETTER_KEY_I;
                break;
            case XK_J:
            case XK_j:
                state->letter_keys |= LETTER_KEY_J;
                break;
            case XK_K:
            case XK_k:
                state->letter_keys |= LETTER_KEY_K;
                break;
            case XK_L:
            case XK_l:
                state->letter_keys |= LETTER_KEY_L;
                break;
            case XK_M:
            case XK_m:
                state->letter_keys |= LETTER_KEY_M;
                break;
            case XK_N:
            case XK_n:
                state->letter_keys |= LETTER_KEY_N;
                break;
            case XK_O:
            case XK_o:
                state->letter_keys |= LETTER_KEY_O;
                break;
            case XK_P:
            case XK_p:
                state->letter_keys |= LETTER_KEY_P;
                break;
            case XK_Q:
            case XK_q:
                state->letter_keys |= LETTER_KEY_Q;
                break;
            case XK_R:
            case XK_r:
                state->letter_keys |= LETTER_KEY_R;
                break;
            case XK_S:
            case XK_s:
                state->letter_keys |= LETTER_KEY_S;
                break;
            case XK_T:
            case XK_t:
                state->letter_keys |= LETTER_KEY_T;
                break;
            case XK_U:
            case XK_u:
                state->letter_keys |= LETTER_KEY_U;
                break;
            case XK_V:
            case XK_v:
                state->letter_keys |= LETTER_KEY_V;
                break;
            case XK_W:
            case XK_w:
                state->letter_keys |= LETTER_KEY_W;
                break;
            case XK_X:
            case XK_x:
                state->letter_keys |= LETTER_KEY_X;
                break;
            case XK_Y:
            case XK_y:
                state->letter_keys |= LETTER_KEY_Y;
                break;
            case XK_Z:
            case XK_z:
                state->letter_keys |= LETTER_KEY_Z;
                break;
            case XK_0:
                state->num_keys |= NUM_KEY_0;
                break;
            case XK_1:
                state->num_keys |= NUM_KEY_1;
                break;
            case XK_2:
                state->num_keys |= NUM_KEY_2;
                break;
            case XK_3:
                state->num_keys |= NUM_KEY_3;
                break;
            case XK_4:
                state->num_keys |= NUM_KEY_4;
                break;
            case XK_5:
                state->num_keys |= NUM_KEY_5;
                break;
            case XK_6:
                state->num_keys |= NUM_KEY_6;
                break;
            case XK_7:
                state->num_keys |= NUM_KEY_7;
                break;
            case XK_8:
                state->num_keys |= NUM_KEY_8;
                break;
            case XK_9:
                state->num_keys |= NUM_KEY_9;
                break;
            case XK_Alt_L:
            case XK_Alt_R:
                state->spec_keys |= SPEC_KEY_ALT;
                break;
            case XK_Control_L:
            case XK_Control_R:
                state->spec_keys |= SPEC_KEY_CTRL;
                break;
            case XK_Return:
                state->spec_keys |= SPEC_KEY_ENTER;
                break;
            case XK_Escape:
                state->spec_keys |= SPEC_KEY_ESCAPE;
                break;
            case XK_space:
                state->spec_keys |= SPEC_KEY_SPACE;
                break;
            case XK_BackSpace:
                state->spec_keys |= SPEC_KEY_BKSP;
                break;
            case XK_Tab:
                state->spec_keys |= SPEC_KEY_TAB;
                break;
            case XK_Delete:
                state->spec_keys |= SPEC_KEY_DEL;
                break;
            default:
                break;
            }
            break;
        case KeyRelease:
            switch (XLookupKeysym(&event.xkey, 0)) {
            case XK_A:
            case XK_a:
                state->letter_keys &= ~LETTER_KEY_A;
                break;
            case XK_B:
            case XK_b:
                state->letter_keys &= ~LETTER_KEY_B;
                break;
            case XK_C:
            case XK_c:
                state->letter_keys &= ~LETTER_KEY_C;
                break;
            case XK_D:
            case XK_d:
                state->letter_keys &= ~LETTER_KEY_D;
                break;
            case XK_E:
            case XK_e:
                state->letter_keys &= ~LETTER_KEY_E;
                break;
            case XK_F:
            case XK_f:
                state->letter_keys &= ~LETTER_KEY_F;
                break;
            case XK_G:
            case XK_g:
                state->letter_keys &= ~LETTER_KEY_G;
                break;
            case XK_H:
            case XK_h:
                state->letter_keys &= ~LETTER_KEY_H;
                break;
            case XK_I:
            case XK_i:
                state->letter_keys &= ~LETTER_KEY_I;
                break;
            case XK_J:
            case XK_j:
                state->letter_keys &= ~LETTER_KEY_J;
                break;
            case XK_K:
            case XK_k:
                state->letter_keys &= ~LETTER_KEY_K;
                break;
            case XK_L:
            case XK_l:
                state->letter_keys &= ~LETTER_KEY_L;
                break;
            case XK_M:
            case XK_m:
                state->letter_keys &= ~LETTER_KEY_M;
                break;
            case XK_N:
            case XK_n:
                state->letter_keys &= ~LETTER_KEY_N;
                break;
            case XK_O:
            case XK_o:
                state->letter_keys &= ~LETTER_KEY_O;
                break;
            case XK_P:
            case XK_p:
                state->letter_keys &= ~LETTER_KEY_P;
                break;
            case XK_Q:
            case XK_q:
                state->letter_keys &= ~LETTER_KEY_Q;
                break;
            case XK_R:
            case XK_r:
                state->letter_keys &= ~LETTER_KEY_R;
                break;
            case XK_S:
            case XK_s:
                state->letter_keys &= ~LETTER_KEY_S;
                break;
            case XK_T:
            case XK_t:
                state->letter_keys &= ~LETTER_KEY_T;
                break;
            case XK_U:
            case XK_u:
                state->letter_keys &= ~LETTER_KEY_U;
                break;
            case XK_V:
            case XK_v:
                state->letter_keys &= ~LETTER_KEY_V;
                break;
            case XK_W:
            case XK_w:
                state->letter_keys &= ~LETTER_KEY_W;
                break;
            case XK_X:
            case XK_x:
                state->letter_keys &= ~LETTER_KEY_X;
                break;
            case XK_Y:
            case XK_y:
                state->letter_keys &= ~LETTER_KEY_Y;
                break;
            case XK_Z:
            case XK_z:
                state->letter_keys &= ~LETTER_KEY_Z;
                break;
            case XK_0:
                state->num_keys &= ~NUM_KEY_0;
                break;
            case XK_1:
                state->num_keys &= ~NUM_KEY_1;
                break;
            case XK_2:
                state->num_keys &= ~NUM_KEY_2;
                break;
            case XK_3:
                state->num_keys &= ~NUM_KEY_3;
                break;
            case XK_4:
                state->num_keys &= ~NUM_KEY_4;
                break;
            case XK_5:
                state->num_keys &= ~NUM_KEY_5;
                break;
            case XK_6:
                state->num_keys &= ~NUM_KEY_6;
                break;
            case XK_7:
                state->num_keys &= ~NUM_KEY_7;
                break;
            case XK_8:
                state->num_keys &= ~NUM_KEY_8;
                break;
            case XK_9:
                state->num_keys &= ~NUM_KEY_9;
                break;
            case XK_Alt_L:
            case XK_Alt_R:
                state->spec_keys &= ~SPEC_KEY_ALT;
                break;
            case XK_Control_L:
            case XK_Control_R:
                state->spec_keys &= ~SPEC_KEY_CTRL;
                break;
            case XK_Return:
                state->spec_keys &= ~SPEC_KEY_ENTER;
                break;
            case XK_Escape:
                state->spec_keys &= ~SPEC_KEY_ESCAPE;
                break;
            case XK_space:
                state->spec_keys &= ~SPEC_KEY_SPACE;
                break;
            case XK_BackSpace:
                state->spec_keys &= ~SPEC_KEY_BKSP;
                break;
            case XK_Tab:
                state->spec_keys &= ~SPEC_KEY_TAB;
                break;
            case XK_Delete:
                state->spec_keys &= ~SPEC_KEY_DEL;
                break;
            default:
                break;
            }
            break;
        case ConfigureNotify:
            glViewport(0, 0, event.xconfigure.width, event.xconfigure.height);
            break;
        default:
            break;
        }
    }

    return;
}
