#ifndef PRIMITIVES_H
#define PRIMITIVES_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <math.h>

/* a coordinate with up to three dimensions */
typedef struct coord {
    double x;
    double y;
    double z;
} coord_t;

/* an angle and axis of rotation */
typedef struct rotate {
    double angle;
    coord_t axis;
} rotate_t;

/* projection constraints */
typedef struct projection {
    double left;
    double right;
    double bottom;
    double top;
    double near_plane;
    double far_plane;
} projection_t;

/* an rgba color value */
typedef struct rgba {
    double red;
    double green;
    double blue;
    double alpha;
} rgba_t;

#define vector_dist(a, b)   sqrt(((b.x - a.x)*(b.x - a.x) + (b.y - a.y)*(b.y - a.y) + (b.z - a.z)*(b.z - a.z)))

/* determine the x/y components of a given vector from normal */
extern void vector_components(double magnitude, double angle, coord_t *components);

/* determine the rotation angle between two components */
extern double vector_rotate(double x, double y);

#ifdef __cplusplus
};
#endif /* __cplusplus */

#endif /* PRIMITIVES_H */
