#ifndef SYSTEM_H
#define SYSTEM_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*
 * expandable list of system implementations
 * the interface should be built with portability in mind
 */
#if defined(SYSTEM_GLX)
#include "system_glx.h"
#elif defined(SYSTEM_CGL)
#include "system_cgl.h"
#elif defined(SYSTEM_WGL)
#include "system_wgl.h"
#else
#error "The current system is not supported"
#endif /* SYSTEM_GLX | SYSTEM_CGL | SYSTEM_WGL */

typedef struct input_state {
    long letter_keys;
    #define LETTER_KEY_A    0x00000001
    #define LETTER_KEY_B    0x00000002
    #define LETTER_KEY_C    0x00000004
    #define LETTER_KEY_D    0x00000008
    #define LETTER_KEY_E    0x00000010
    #define LETTER_KEY_F    0x00000020
    #define LETTER_KEY_G    0x00000040
    #define LETTER_KEY_H    0x00000080
    #define LETTER_KEY_I    0x00000100
    #define LETTER_KEY_J    0x00000200
    #define LETTER_KEY_K    0x00000400
    #define LETTER_KEY_L    0x00000800
    #define LETTER_KEY_M    0x00001000
    #define LETTER_KEY_N    0x00002000
    #define LETTER_KEY_O    0x00004000
    #define LETTER_KEY_P    0x00008000
    #define LETTER_KEY_Q    0x00010000
    #define LETTER_KEY_R    0x00020000
    #define LETTER_KEY_S    0x00040000
    #define LETTER_KEY_T    0x00080000
    #define LETTER_KEY_U    0x00100000
    #define LETTER_KEY_V    0x00200000
    #define LETTER_KEY_W    0x00400000
    #define LETTER_KEY_X    0x00800000
    #define LETTER_KEY_Y    0x01000000
    #define LETTER_KEY_Z    0x02000000

    short num_keys;
    #define NUM_KEY_0       0x0001
    #define NUM_KEY_1       0x0002
    #define NUM_KEY_2       0x0004
    #define NUM_KEY_3       0x0008
    #define NUM_KEY_4       0x0010
    #define NUM_KEY_5       0x0020
    #define NUM_KEY_6       0x0040
    #define NUM_KEY_7       0x0080
    #define NUM_KEY_8       0x0100
    #define NUM_KEY_9       0x0200

    char spec_keys;
    #define SPEC_KEY_ALT    0x01
    #define SPEC_KEY_CTRL   0x02
    #define SPEC_KEY_ENTER  0x04
    #define SPEC_KEY_ESCAPE 0x08
    #define SPEC_KEY_SPACE  0x10
    #define SPEC_KEY_BKSP   0x20
    #define SPEC_KEY_TAB    0x40
    #define SPEC_KEY_DEL    0x80
} input_state_t;

/* initialize the graphics subsystem */
extern void system_init(unsigned int xres, unsigned int yres, const char *title);

/* free the graphics subsystem */
extern void system_free(void);

/* swap frames if multibuffered */
extern void system_gfx_swap(void);

/* toggle to fullscreen */
extern void system_gfx_togglefs(void);

/* do a poll of the input events */
extern void system_input_poll(input_state_t *state);

#ifdef __cplusplus
};
#endif /* __cplusplus */

#endif /* SYSTEM_H */
