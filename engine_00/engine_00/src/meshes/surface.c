#include <stddef.h>

#if defined(SYSTEM_GLX)
#include <GL/gl.h>
#elif defined(SYSTEM_CGL)
#include <OpenGL/gl.h>
#elif defined(SYSTEM_WGL)
#include <Windows.h>
#include <gl/GL.h>
#else
#error "The current system is not supported"
#endif /* SYSTEM_GLX | SYSTEM_CGL | SYSTEM_WGL */

#include "../system/primitives.h"

#include "../scene/mesh.h"

static coord_t vertices_surface[] = {
    {  20.0, -1.0,  20.0  },
    {  20.0, -1.0, -20.0  },
    { -20.0, -1.0, -20.0  },
    { -20.0, -1.0,  20.0  }
};

static coord_t normals_surface[] = {
    { 0.0, 1.0, 0.0 },
    { 0.0, 1.0, 0.0 },
    { 0.0, 1.0, 0.0 },
    { 0.0, 1.0, 0.0 }
};

static rgba_t colors_surface[] = {
    { 1.0, 0.0, 0.0, 1.0 },
    { 0.0, 1.0, 0.0, 1.0 },
    { 0.0, 0.0, 1.0, 1.0 },
    { 0.0, 1.0, 1.0, 1.0 }
};

mesh_t mesh_surface = {
    vertices_surface,
    normals_surface,
    colors_surface,
    4,
    GL_QUADS
};
