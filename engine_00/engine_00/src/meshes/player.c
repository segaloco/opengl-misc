#include <stddef.h>

#if defined(SYSTEM_GLX)
#include <GL/gl.h>
#elif defined(SYSTEM_CGL)
#include <OpenGL/gl.h>
#elif defined(SYSTEM_WGL)
#include <Windows.h>
#include <gl/GL.h>
#else
#error "The current system is not supported"
#endif /* SYSTEM_GLX | SYSTEM_CGL | SYSTEM_WGL */

#include "../system/primitives.h"

#include "../scene/mesh.h"

static coord_t vertices_player[] = {
    {  0.0,  1.0,  0.0  },
    { -1.0, -1.0,  1.0  },
    {  1.0, -1.0,  1.0  },

    {  0.0,  1.0,  0.0  },
    {  1.0, -1.0,  1.0  },
    {  0.0, -1.0, -1.0  },

    {  0.0,  1.0,  0.0  },
    {  0.0, -1.0, -1.0  },
    { -1.0, -1.0,  1.0  },

    {  0.0, -1.0, -1.0  },
    {  1.0, -1.0,  1.0  },
    { -1.0, -1.0,  1.0  }
};

static rgba_t colors_player[] = {
    { 1.0, 1.0, 0.0, 1.0 },
    { 0.0, 1.0, 1.0, 1.0 },
    { 1.0, 0.0, 1.0, 1.0 },

    { 1.0, 1.0, 0.0, 1.0 },
    { 0.0, 1.0, 1.0, 1.0 },
    { 1.0, 0.0, 1.0, 1.0 },

    { 1.0, 1.0, 0.0, 1.0 },
    { 0.0, 1.0, 1.0, 1.0 },
    { 1.0, 0.0, 1.0, 1.0 },

    { 1.0, 1.0, 0.0, 1.0 },
    { 0.0, 1.0, 1.0, 1.0 },
    { 1.0, 0.0, 1.0, 1.0 }
};

mesh_t mesh_player = {
    vertices_player,    /* coord_t  *vertices       */
    NULL,               /* coord_t  *normals        */
    colors_player,      /* rgba_t   *colors         */
    12,                 /* size_t   vertex_count    */
    GL_TRIANGLES        /* GLenum   mode            */
};
