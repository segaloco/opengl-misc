#ifndef MESHES_H
#define MESHES_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "../scene/mesh.h"

extern mesh_t mesh_surface;
extern mesh_t mesh_player;
extern mesh_t mesh_cube;
extern mesh_t mesh_diamond;

#ifdef __cplusplus
};
#endif /* __cplusplus */

#endif /* MESHES_H */
